#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'James Iter'
__date__ = '15/5/31'
__contact__ = 'james.iter.cn@gmail.com'
__copyright__ = '(c) 2015 by James Iter.'

import os
from initialize import es

es.config['HTTP'] = 'http://'
es.config['HTTPS'] = 'https://'
es.config['DOMAIN'] = 'ez-iso.com'
es.config['HOSTNAME'] = 'ezs'
es.config['FQDN'] = '.'.join([es.config['HOSTNAME'], es.config['DOMAIN']])
es.config['BASE_URL'] = ''.join([es.config['HTTP'], es.config['FQDN']])
es.config['BASES_URL'] = ''.join([es.config['HTTPS'], es.config['FQDN']])

es.config['SLOT_SIZE'] = 65536
es.config['FDFS_CONFIG_PATH'] = ''.join([os.getcwd(), '/../configs/fdfs_client.conf'])
es.config['DEBUG'] = True
es.config['LOG_FILE_BASE'] = ''.join([os.getcwd(), '/logs/log'])
es.config['UPLOAD_FOLDER'] = ''.join([os.getcwd(), '/uploads'])
es.config['TMP_STORAGE_PATH'] = ''.join([os.getcwd(), '/storage/tmp'])
es.config['ALLOWED_EXTENSIONS'] = {'txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif', 'mov'}
es.config['ALLOWED_IMAGE_MIME'] = {'image/gif', 'image/jpeg', 'image/png', 'image/tiff', 'image/vnd.wap.wbmp',
                                   'image/x-icon', 'image/x-jng', 'image/x-ms-bmp', 'image/svg+xml', 'image/webp'}
es.config['ALLOWED_VIDEO_MIME'] = {'video/3gpp', 'video/mp2t', 'video/mp4', 'video/mpeg', 'video/quicktime',
                                   'video/webm', 'video/x-flv', 'video/x-m4v', 'video/x-mng', 'video/x-ms-asf',
                                   'video/x-ms-wmv', 'video/x-msvideo'}
es.config['GM_BIN'] = '/usr/bin/gm'
es.config['LIBAV_BIN'] = '/usr/bin/avconv'

es.config['DIR_NAME_LENGTH_MIN'] = 3
es.config['DIR_NAME_LENGTH_MAX'] = 63
es.config['DIR_DEPTH_MAX'] = 5

es.config['FILE_NAME_LENGTH_MIN'] = 3
es.config['FILE_NAME_LENGTH_MAX'] = 128

es.config['BUCKET_LENGTH_MIN'] = 3
es.config['BUCKET_LENGTH_MAX'] = 63
es.config['RESOURCE_PATH_LENGTH_MAX'] = 1024

es.config['BATCH_OPERATE_OBJECT_MIN'] = 1
es.config['BATCH_OPERATE_OBJECT_MAX'] = 128

es.config['ACCESS_KEY_ID_LENGTH'] = 20
es.config['ACCESS_KEY_SECRET_LENGTH'] = 40

es.config['MESSAGE_RESEND_TTL'] = 60 * 5                                    # 5分钟
es.config['MOBILE_LENGTH'] = 11
es.config['SMS_LENGTH_MIN'] = 1
es.config['SMS_LENGTH_MAX'] = 280
es.config['EMAIL_STRING_LENGTH_MIN'] = 6
es.config['EMAIL_STRING_LENGTH_MAX'] = 64

es.config['LOGIN_NAME_UID_MAPPING_KEY'] = 'H:LoginNameMUID'     # 登录名(email)与uid的映射key
es.config['ACCOUNT_KEY_HEADER'] = 'H:Account:'                  # 记录账号信息的key的头部

es.config['LONG_MIN'] = 0
es.config['LONG_MAX'] = 9223372036854775807

es.config['UID_MIN'] = 1
es.config['UID_MAX'] = es.config['LONG_MAX']

es.config['LOGIN_NAME_LENGTH_MIN'] = 5
es.config['LOGIN_NAME_LENGTH_MAX'] = 20

es.config['PASSWORD_MIN'] = 8
es.config['PASSWORD_MAX'] = 64

es.config['ROLE_GROUP_NAME_GID_MAPPING_KEY'] = 'H:RoleGroupNameMGID'    # RoleGroup名(组名)与gid的映射key
es.config['ROLE_GROUP_KEY_HEADER'] = 'H:RoleGroup:'                     # 记录RoleGroup信息的key的头部
es.config['ROLE_GROUP_UID_MAPPING_KEY'] = 'S:RoleGroupMUID:'            # RoleGroup的成员uid集合
es.config['ROLE_GROUP_PRIVILEGES_MAPPING_KEY'] = 'S:RoleGroupMPrivileges:'            # RoleGroup的特权集合
es.config['GID_MIN'] = 1
es.config['GID_MAX'] = es.config['LONG_MAX']

es.config['UID_ROLE_GROUP_MAPPING_KEY'] = 'S:UIDMGID:'                  # 记录账号所在的组集合

es.config['PRIVILEGE_LABEL_MAPPING_KEY'] = 'H:PrivilegeMLabel'          # Privilege(特权)与Label的映射key
es.config['LABEL_PRIVILEGE_MAPPING_KEY'] = 'H:LabelMPrivilege'          # Label与Privilege(特权)的映射key

es.config['SESSION_TTL'] = 1800
es.config['TOKEN_TTL'] = 3600 * 24                                      # 单位(秒)
es.config['TOKEN_AUTO_RENEW_TIME_LEFT'] = 3600                          # 自动更新token的剩余时间 单位(秒)
es.config['TOKEN_KEY_HEADER'] = 'V:Token:'                              # 用户token的key头部

es.config['NORMAL_STRING_FIELD_MIN'] = 1
es.config['NORMAL_STRING_FIELD_MAX'] = 256

es.config['INDEX_NODE_MIN'] = 1
es.config['INDEX_NODE_MAX'] = es.config['LONG_MAX']

es.config['POSITION_MIN'] = 0
es.config['POSITION_MAX'] = es.config['LONG_MAX']
es.config['POSITION_GAP_MAX'] = 100

es.config['DIR_KEY_HEADER'] = 'Z:Dir:'
es.config['FILE_KEY_HEADER'] = 'H:File:'
es.config['RESOURCE_KEY_HEADER'] = 'H:Resource:'

es.config['INDEX_NODE_TREE_HEADER'] = 'Z:iNodeTree:'
es.config['INDEX_NODE_METADATA_HEADER'] = 'H:iNodeMetadata:'
es.config['NAME_INDEX_NODE_MAPPING_HEADER'] = 'H:NameMiNode:'
es.config['PATH'] = ''.join(['^(/[^/ &?,\*!]{1,256}){1,16}$'])              # 文件名长度1-256字节，深度1-16

es.config['TENANTRY_KEY_HEADER'] = 'H:Tenantry:'                            # 记录租户信息的key的头部
es.config['BUCKET_SET_BY_UID_KEY_HEADER'] = 'S:BucketByUID:'                # 记录uid所拥有的bucket集合key的头部
es.config['BUCKET_KEY_HEADER'] = 'H:Bucket:'                                # 记录bucket对象的key头部
es.config['BUCKET_SET'] = 'S:Buckets'                                       # 系统中所有bucket的集合
es.config['BUCKET_NAME_PATTERN'] = ''.join(['^[\w-]{', str(es.config['BUCKET_LENGTH_MIN']), ',',
                                            str(es.config['BUCKET_LENGTH_MAX']), '}$'])

es.config['SPEC'] = '(@.{1,100})*'
es.config['RESOURCE_PATH_PATTERN'] = ''.join([es.config['BUCKET_NAME_PATTERN'][:-1], '(/[^/ &?,\*!]{',
                                              str(es.config['FILE_NAME_LENGTH_MIN']), ',',
                                              str(es.config['FILE_NAME_LENGTH_MAX']), '})',
                                              '{1,', str(es.config['DIR_DEPTH_MAX']), '}', es.config['SPEC'], '$'])

es.config['ACCESS_KEY_SET_BY_UID_KEY_HEADER'] = 'S:AccessKeyByUID:'            # 记录uid所拥有的access key集合key的头部
es.config['ACCESS_KEY_HEADER'] = 'H:AccessKey:'                                # 记录access key对象的key头部
es.config['ACCESS_KEY_SET'] = 'S:AccessKeys'                                   # 系统中所有access key的集合

es.config['INVITATION_TENANTRY_KEY_HEADER'] = 'V:InvitationTenantry:'       # 邀请租户key的头部
es.config['INVITATION_TENANTRY_TOKEN_TTL'] = 3600 * 24                      # 单位(秒)
es.config['INVITATION_TENANTRY_KEY_TIMEOUT'] = 3600 * 24                    # 单位(秒)
es.config['INVITATION_TENANTRY_VERIFY_URL'] = ''                            # 租户邀请函中的有效性校验URL
es.config['INVITATION_TENANTRY_COPYWRITTEN_TITLE'] = '请验证您的账户'         # 租户邀请函文案标题
es.config['INVITATION_TENANTRY_COPYWRITTEN'] = "''.join(['点击', es.config['INVITATION_TENANTRY_VERIFY_URL'], " \
                                               "'完善您的资料!'])"            # 租户邀请函文案

es.config['IDsKeeper'] = 'H:IDsKeeper'

es.config['DEFAULT_MAIL_SENDER'] = '@'.join(['do_not_reply', es.config['DOMAIN']])
es.config['SMTP_SERVER'] = 'smtp.mailgun.org'
es.config['SMTP_STARTTLS'] = True
es.config['SMTP_PORT'] = None                                           # 值为None时，让程序通过SMTP_STARTTLS来自动识别
es.config['SMTP_LOGIN_NAME'] = os.environ.get('SMTP_LOGIN_NAME', 'postmaster@ez-iso.com')
es.config['SMTP_PASSWORD'] = os.environ.get('SMTP_PASSWORD', '1420aca7e76734014f7eacb7511090e1')
# 邮件不区分大小写，地址部分起始必须为字母或数字，句点(.)和下划线(_)不可以组合及连续出现，邮件域部分遵循域名命名规则
es.config['EMAIL_PATTERN'] = '^[a-z0-9]{1,20}([._][a-z0-9]{1,20}){0,5}@' \
                             '[a-z0-9]{1,20}([-][a-z0-9]{1,20}){0,2}\.[a-z]{1,5}$'

# MG for mailgun
es.config['MG_API_BASE_URL'] = ''.join(['https://api.mailgun.net/v3/', es.config['DOMAIN'], '/messages'])
es.config['MG_API_KEY'] = 'key-8hesvigb0y368qpzkpzoh5eiknu1jhx6'

es.config['PUSH_QUEUE'] = 'Q:PushQueue'
es.config['PUSH_QUEUE_HOST'] = '.'.join(['push.queue', es.config['DOMAIN']])

es.config['JWT_SECRET'] = os.environ.get('JWT_SECRET', '')
es.config['JWT_ALGORITHM'] = 'HS512'
es.config['JWT_OPTIONS'] = {                                            # 默认jwt decode检验选项
    'verify_signature': True,
    'verify_exp': True,
    'verify_nbf': True,
    'verify_iat': True,
    'verify_aud': True,
    'require_exp': False,
    'require_iat': False,
    'require_nbf': False
}

es.config['IMAGE_QUEUE_ACCEPT'] = 'Q:ImageQueueAccept'
es.config['IMAGE_PROCESSING_SET'] = 'S:ImageQueueProcessing'
es.config['VIDEO_QUEUE'] = 'Q:VideoQueue'
es.config['ABNORMAL_SET'] = 'Z:Abnormal'                                # 异常的资源集和{'': , ''}

es.config['GRAVITY_MAP'] = {
    'nw': 'NorthWest',
    'n': 'North',
    'ne': 'NorthEast',
    'w': 'West',
    'c': 'Center',
    'e': 'East',
    'sw': 'SouthWest',
    's': 'South',
    'se': 'SouthEast'
}