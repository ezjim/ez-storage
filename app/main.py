#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'James Iter'
__date__ = '15/5/31'
__contact__ = 'james.iter.cn@gmail.com'
__copyright__ = '(c) 2015 by James Iter.'

import traceback
import json
import sys
from flask import request, make_response
from app.initialize import *
from app.models import Tenantry
import route_table

from app.views.db import blueprint as db_blueprint
from app.views.RBAC import blueprint as RBAC_blueprint
from app.views.account import blueprint as account_blueprint
from app.views.tenantry import blueprint as tenantry_blueprint
from app.views.bucket import blueprint as bucket_blueprint
from app.views.access_key import blueprint as access_key_blueprint
from app.views.organize import blueprint as organize_blueprint
from app.views.resource import blueprint as resource_blueprint


reload(sys)
sys.setdefaultencoding('utf8')

"""
'r_' for route
"""

if __name__ == '__main__':
    # noinspection PyBroadException
    try:
        es.register_blueprint(db_blueprint)
        es.register_blueprint(RBAC_blueprint)
        es.register_blueprint(account_blueprint)
        es.register_blueprint(tenantry_blueprint)
        es.register_blueprint(bucket_blueprint)
        es.register_blueprint(access_key_blueprint)
        es.register_blueprint(organize_blueprint)
        es.register_blueprint(resource_blueprint)
        es.run(host='127.0.0.1', port=8888)
    except:
        logger.error(traceback.format_exc())

