#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'James Iter'
__date__ = '15/7/1'
__contact__ = 'james.iter.cn@gmail.com'
__copyright__ = '(c) 2015 by James Iter.'

import requests
from app.initialize import *
from app.models import states
import json
import hashlib

process_title = os.path.basename(__file__).split('.')[0]
log_file_path = '.'.join([es.config['LOG_FILE_BASE'], process_title])
administrators = ['15601603670']

logger = logging.getLogger(process_title)

if es.config['DEBUG']:
    logger.setLevel(logging.DEBUG)
else:
    logger.setLevel(logging.WARNING)

fh = TimedRotatingFileHandler(log_file_path, when="W0", interval=1, backupCount=2)
# a rollover occurs.  Current 'when' events supported:
# S - Seconds
# M - Minutes
# H - Hours
# D - Days
# midnight - roll over at midnight
# W{0-6} - roll over on a certain day; 0 - Monday
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(funcName)s - %(lineno)s - %(message)s')
fh.setFormatter(formatter)
logger.addHandler(fh)

r_push = r

"""
Reference: [
    https://documentation.mailgun.com/user_manual.html#sending-via-api
]
"""


def send_email_by_http(payload):
    data = {'from': payload['sender'], 'to': payload['receivers'], 'subject': payload['title'],
            'html': payload['message']}
    data.update(payload['others'])
    return requests.post(es.config['MG_API_BASE_URL'], auth=("api", es.config['MG_API_KEY']), data=data)


def send_sms_to(admin, msg):
    pass

while True:
    if r_push.llen(es.config['PUSH_QUEUE']) > 5:
        send_sms_to(administrators, ''.join(['短信队列异常，当前长度: ', str(r_push.llen(es.config['PUSH_QUEUE']))]))
        exit()

    k, payload_ = r_push.blpop([es.config['PUSH_QUEUE']])
    if k == es.config['PUSH_QUEUE']:
        try:
            payload_ = json.loads(payload_)
        except Exception, e:
            msg_ = ''.join([e.message, ' --> RAW: ', str(payload_)])

        channel = payload_.get('channel', None)
        if channel not in states.MessageChannel.__members__.values():
            logger.error(''.join(['无法识别的消息通道: ', str(channel), '. --> RAW: ', str(payload_)]))
            continue

        payload_hash = hashlib.sha1(str(payload_)).hexdigest()
        payload_resend_path = ''.join(['V:MessageSent:', payload_hash])
        if not r_push.exists(payload_resend_path):
            r_push.setex(payload_resend_path, es.config['MESSAGE_RESEND_TTL'], str(payload_))

            if channel == states.MessageChannel.email.value:
                ret = send_email_by_http(payload_)
                if 200 != ret.status_code:
                    logger.warning(ret.text)
