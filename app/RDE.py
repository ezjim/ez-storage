#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'James Iter'
__date__ = '15/8/1'
__contact__ = 'james.iter.cn@gmail.com'
__copyright__ = '(c) 2015 by James Iter.'

"""
    RDE for resource dispose engine
"""

import json
import multiprocessing
import os
import sys
import commands
import hashlib
import urllib
import base64
import jimit as ji

sys.path.append("..")

from app.initialize import *
from app.models import ResourceBasic, ResourceChild, Image, Rules

reload(sys)
sys.setdefaultencoding('utf8')

process_title = os.path.basename(__file__).split('.')[0]
log_file_path = '.'.join([es.config['LOG_FILE_BASE'], process_title])

logger = logging.getLogger(process_title)

if es.config['DEBUG']:
    logger.setLevel(logging.DEBUG)
else:
    logger.setLevel(logging.WARNING)

fh = TimedRotatingFileHandler(log_file_path, when="W0", interval=1, backupCount=2)
# a rollover occurs.  Current 'when' events supported:
# S - Seconds
# M - Minutes
# H - Hours
# D - Days
# midnight - roll over at midnight
# W{0-6} - roll over on a certain day; 0 - Monday
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(funcName)s - %(lineno)s - %(message)s')
fh.setFormatter(formatter)
logger.addHandler(fh)


def shell_cmd(cmd):
    try:
        exit_status, output = commands.getstatusoutput(cmd)
    except Exception, e:
        logger.error(e.message)
        logger.error(''.join(['命令执行出错: ', str(cmd)]))
        return False

    if exit_status != 0:
        logger.error(''.join(['命令执行退出异常: ', str(output)]))
        return False

    return True

def post_spec_resource(resource_id=None, path=None, spec=None):
    """ 于原始资源置入指定规格资源(选用post有覆盖原有资源的意思，put每次都应该产生新事物)
    :param resource_id: 原始资源key
    :param path: 指定规格资源的存放路径
    :param spec: 指定规格格式化过的字符串
    :return: None
    """
    args_rules = [
        Rules.RESOURCE_ID.value,
        Rules.PATH.value,
        Rules.SPEC.value
    ]

    ret = ji.Check.previewing(args_rules, locals())

    if not os.path.isfile(path=path):
        ret['state'] = ji.Common.exchange_state(50051)
        ret['state']['zh-cn'] = ''.join([ret['state']['zh-cn'], ': ', str(path)])
        raise ji.PreviewingError(json.dumps(ret, ensure_ascii=False))

    resource_child = ResourceChild(ancestor_resource_id=resource_id)
    resource_child.resource_id = ji.Common.calc_sha1_by_file(path)
    resource_child.size = os.path.getsize(path)
    fs_ret = fs_client.upload_by_filename(filename=path)
    file_id = fs_ret['Remote file_id']
    resource_child.object_id = file_id
    resource_child.create()

    origin_resource_key_path = ''.join([es.config['RESOURCE_KEY_HEADER'], resource_id])
    r.hset(origin_resource_key_path, spec, resource_child.resource_id)
    r.srem(es.config['IMAGE_PROCESSING_SET'], spec)

def rde_worker():
    """
    RDE for resource dispose engine
    备注: 从队列中读取的队列请求，在这里不做任何已存在资源的判断。即：如果已存在，则覆盖。
    """
    while True:
        k, v = r.brpop([es.config['IMAGE_QUEUE_ACCEPT'], es.config['VIDEO_QUEUE']])
        # v: "{'resource_id': resource_id, 'spec': spec}"
        # 处理完从其中删除
        r.sadd(es.config['IMAGE_PROCESSING_SET'], v)
        content = json.loads(v)
        args_rules = [
            Rules.RESOURCE_ID.value,
            Rules.SPEC.value
        ]

        try:
            ji.Check.previewing(args_rules, content)
        except ji.JITError, e:
            logger.warning(e.message)
            logger.warning(json.dumps(content, ensure_ascii=False))
            continue

        spec_formatted = content['spec']
        spec_structure = ResourceBasic.spec_parse(spec_formatted)

        resource_obj = ResourceBasic(resource_id=content['resource_id'])
        if not resource_obj.exist():
            logger.warning(''.join(['不存在的资源id: ', resource_obj.resource_id]))
            logger.warning(json.dumps(content, ensure_ascii=False))
            continue

        resource_obj.get()
        tmp_origin = os.path.join(es.config['TMP_STORAGE_PATH'], resource_obj.resource_id[-1],
                                  resource_obj.resource_id).encode('utf8')
        tmp_dest = '_'.join([tmp_origin, spec_formatted]).encode('utf8')
        watermark_image_path = None

        try:
            fs_client.download_to_file(local_filename=tmp_origin, remote_file_id=str(resource_obj.object_id))
        except Exception, e:
            logger.error(e.message)
            logger.error(''.join(['FDFS下载错误，相关resource_obj: ',
                                  json.dumps(resource_obj.__dict__, ensure_ascii=False)]))
            continue

        if k == es.config['IMAGE_QUEUE_ACCEPT']:
            cmd = ''.join([es.config['GM_BIN'], ' convert ', tmp_origin, ' -strip -gravity center'])
            for item in ['basic', 'effects', 'watermark']:
                if item not in spec_structure:
                    continue

                if item == 'basic':
                    spec_basic = spec_structure[item]
                    """
                    http://www.graphicsmagick.org/GraphicsMagick.html#details-geometry
                    http://www.graphicsmagick.org/Magick++/Geometry.html
                    '^': 默认等比缩放以小边作为参考，该参数切换为以大边作为参考
                    'x': 默认取 宽x高 较小值作为等比缩放的参考。如果在没包含'x'时仅指定宽或高，那么宽、高将相等(即：100表示100x100)。
                            如果联合'x'，仅指定宽或高，那么另一个值将按原图比例计算得出(即：如果原图640x480，x120表示160x120；
                            32x表示32x24)
                    '>': 仅在图片尺寸大于指定缩放值时起作用，等比按小边缩小(即：如果原图640x480，16x600>表示16x12，700x500>表示640x480)
                    '<': 当图片尺寸小于指定缩放值时起作用，等比按小边放大(即：如果原图为640x480，720x500<表示667×500, 720x50<表示640x480)
                    '!': 任意拉伸，等比失效(即：如果原图为640x480，720x300!表示720×300)
                    """
                    cmd = ''.join([cmd, ' -quality ', str(spec_basic.get('q', '85'))])

                    dimensions = ''.join([str(spec_basic.get('w', '')), 'x', str(spec_basic.get('h', ''))])

                    if 'w' in spec_basic and 'h' in spec_basic:
                        cmd = ''.join([cmd, ' -resize "', dimensions, '^>" -background black -extent ', dimensions])

                    elif 'w' in spec_basic or 'h' in spec_basic:
                        cmd = ''.join([cmd, ' -resize "', dimensions, '>"'])
                    else:
                        pass

                    if 'r' in spec_basic:
                        cmd = ''.join([cmd, ' -rotate ', str(spec_basic['r'])])

                elif item == 'effects':
                    spec_effects = spec_structure[item]

                    if 'cs' in spec_effects:
                        cmd = ''.join([cmd, ' -colorspace ', str(spec_effects['cs'])])

                    if 'gb' in spec_effects:
                        cmd = ''.join([cmd, ' -filter Gaussian -blur ', str(spec_effects['gb'])])

                elif item == 'watermark':
                    spec_watermark = spec_structure[item]

                    if 'text' in spec_watermark:
                        font_point_size = 72
                        canvas_height = font_point_size
                        spec_watermark['text'] = urllib.unquote(spec_watermark['text'])
                        text_hash = hashlib.sha256(spec_watermark['text']).hexdigest()
                        watermark_image_path = os.path.join(es.config['TMP_STORAGE_PATH'], text_hash[-1],
                                                            ''.join([text_hash, '.png']))

                        if 'expand-per' in spec_watermark:
                            canvas_height = canvas_height * int(spec_watermark['expand-per']) / 100

                        # TODO: 考虑更合理的实现字体选项
                        text_watermark_cmd = ''.join([es.config['GM_BIN'], ' convert -size ',
                                                      str(canvas_height * spec_watermark['text'].__len__() / 2), 'x',
                                                      str(canvas_height), ' xc:transparent -format PNG -gravity Center '
                                                      '-font assets/fonts/华文黑体.ttf'])

                        text_watermark_cmd = ''.join([text_watermark_cmd, ' -pointsize ', str(font_point_size)])

                        if 'font-color' in spec_watermark:
                            text_watermark_cmd = ''.join([text_watermark_cmd, ' -fill ', spec_watermark['font-color']])

                        if 'stroke-color' in spec_watermark:
                            text_watermark_cmd = ''.join([text_watermark_cmd, ' -stroke',
                                                          spec_watermark['stroke-color']])

                        if 'stroke-width' in spec_watermark:
                            text_watermark_cmd = ''.join([text_watermark_cmd, ' -strokewidth',
                                                          spec_watermark['stroke-width']])

                        text_watermark_cmd = ''.join([text_watermark_cmd, ' -draw "', 'text 0,0 \'',
                                                      spec_watermark['text'], '\'" ', watermark_image_path])

                        if not shell_cmd(cmd=text_watermark_cmd):
                            continue

                    elif 'image-path' in spec_watermark:
                        # 判断图片资源是否存在于当前存储中 在spec_parse当中已做
                        from app.models import File, Utils
                        try:
                            bucket, path, _spec = Utils.separate_bucket_path_spec(
                                resource_path=base64.b32decode(spec_watermark['image-path'].upper()))
                            _file = File()
                            _file.bucket = bucket
                            _file.get_by_path(path=path)
                            resource_obj_by_watermark = ResourceBasic(resource_id=_file.resource_id)
                            resource_obj_by_watermark.get()
                        except Exception, e:
                            logger.error(e.message)
                            logger.error(''.join(['image-path: ', spec_watermark['image-path']]))
                            continue

                        watermark_image_path = os.path.join(es.config['TMP_STORAGE_PATH'],
                                                            resource_obj_by_watermark.resource_id[-1],
                                                            ''.join([resource_obj_by_watermark.resource_id, '.png']))
                        try:
                            fs_client.download_to_file(local_filename=watermark_image_path,
                                                       remote_file_id=str(resource_obj_by_watermark.object_id))
                        except Exception, e:
                            logger.error(e.message)
                            logger.error(''.join(['FDFS下载错误，相关resource_obj: ',
                                                  json.dumps(resource_obj_by_watermark.__dict__, ensure_ascii=False)]))
                            continue

                else:
                    logger.warning(''.join(['未支持的规格类型: ', str(item), ', 于: ', str(v)]))

            cmd = ''.join([cmd, ' ', tmp_dest])

        elif k == es.config['VIDEO_QUEUE']:
            cmd = ''
        else:
            logger.warning(''.join(['未识别的队列: ', str(k), ', 消息内容: ', str(v)]))
            continue

        if not os.path.isfile(tmp_origin):
            logger.warning(''.join(['未知路径: ', str(tmp_origin)]))
            continue

        if not shell_cmd(cmd=cmd):
            continue

        if watermark_image_path is not None:
            watermark_merge_cmd = ''.join([es.config['GM_BIN'], ' composite'])

            if 'resize-per' in spec_watermark:
                watermark_merge_cmd = ''.join([watermark_merge_cmd, ' -resize ', spec_watermark['resize-per'], '%'])

            if 'gravity' not in spec_watermark:
                watermark_merge_cmd = ''.join([watermark_merge_cmd, ' -gravity Center'])
            else:
                watermark_merge_cmd = ''.join([watermark_merge_cmd,
                                               ' -gravity ', es.config['GRAVITY_MAP'][spec_watermark['gravity']]])

            if 'dissolve' not in spec_watermark:
                watermark_merge_cmd = ''.join([watermark_merge_cmd, ' -dissolve 20'])
            else:
                watermark_merge_cmd = ''.join([watermark_merge_cmd, ' -dissolve ', spec_watermark['dissolve']])

            watermark_merge_cmd = ' '.join([watermark_merge_cmd, watermark_image_path, tmp_dest, tmp_dest])

            if not shell_cmd(watermark_merge_cmd):
                continue

        try:
            post_spec_resource(resource_id=resource_obj.resource_id, path=tmp_dest, spec=spec_formatted)
        except Exception, e:
            logger.error(''.join(['指定规格资源置入异常: ', str(e.message)]))
            continue

        os.remove(tmp_origin)
        os.remove(tmp_dest)


pool = multiprocessing.Pool(processes=multiprocessing.cpu_count())

for i in xrange(multiprocessing.cpu_count()):
    pool.apply_async(rde_worker())

pool.close()
pool.join()
