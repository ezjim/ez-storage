#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'James Iter'
__date__ = '15/5/31'
__contact__ = 'james.iter.cn@gmail.com'
__copyright__ = '(c) 2015 by James Iter.'


from flask import Flask
from fdfs_client.client import Fdfs_client
import redis
import logging
from logging.handlers import TimedRotatingFileHandler

import jimit as ji
from state_code import *


es = Flask(__name__)
from config import *

ji.state_code.index_state['branch'] = dict(ji.state_code.index_state['branch'], **own_state_branch)

log_dir = os.path.dirname(es.config['LOG_FILE_BASE'])
if not os.path.isdir(log_dir):
    os.makedirs(log_dir, 0755)

process_title = 'ez-storage'
log_file_path = '.'.join([es.config['LOG_FILE_BASE'], process_title])
logger = logging.getLogger(log_file_path)

if es.config['DEBUG']:
    logger.setLevel(logging.DEBUG)
else:
    logger.setLevel(logging.WARNING)

fh = TimedRotatingFileHandler(log_file_path, when="D", interval=1, backupCount=7)
# a rollover occurs.  Current 'when' events supported:
# S - Seconds
# M - Minutes
# H - Hours
# D - Days
# midnight - roll over at midnight
# W{0-6} - roll over on a certain day; 0 - Monday
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(funcName)s - %(lineno)s - %(message)s')
fh.setFormatter(formatter)
logger.addHandler(fh)

fs_client = Fdfs_client(es.config['FDFS_CONFIG_PATH'])

if 'REDIS_PSWD' in os.environ:
    es.config['REDIS_PSWD'] = os.environ['REDIS_PSWD']

r = redis.StrictRedis(host='127.0.0.1', port=2301, db=0, decode_responses=True)
try:
    r.ping()
except redis.exceptions.ResponseError:
    r = redis.StrictRedis(host='127.0.0.1', port=2301, db=0, password=es.config['REDIS_PSWD'], decode_responses=True)

"""
smtp_server = ji.NetUtils.smtp_init(host=es.config['SMTP_SERVER'], port=es.config['SMTP_PORT'],
                                    login_name=es.config['SMTP_LOGIN_NAME'], password=es.config['SMTP_PASSWORD'],
                                    tls=es.config['SMTP_STARTTLS'])
"""


def init_storage_path():
    if not os.path.isdir(es.config['TMP_STORAGE_PATH']):
        for i in range(0, 16):
            path = os.path.join(es.config['TMP_STORAGE_PATH'], format(i, 'x'))
            os.makedirs(path, 0755)

init_storage_path()
