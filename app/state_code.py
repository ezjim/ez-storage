#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'James Iter'
__date__ = '15/5/31'
__contact__ = 'james.iter.cn@gmail.com'
__copyright__ = '(c) 2015 by James Iter.'


own_state_branch = {
    # ----------以上将集成至jimit库中------------
    '20201': {
        'code': '20201',
        'zh-cn': u'新规格的资源生成已接受'
    },
    '41250': {
        'code': '41250',
        'zh-cn': u'文件上传不完整'
    },
    '41251': {
        'code': '41251',
        'zh-cn': u'资源路径有误'
    },
    '50050': {
        'code': '50050',
        'zh-cn': u'bucket index node自增id异常'
    },
    '50051': {
        'code': '50051',
        'zh-cn': u'路径不存在'
    },
    '50052': {
        'code': '50052',
        'zh-cn': u'邮件发送失败'
    },
    '50053': {
        'code': '50053',
        'zh-cn': u'Redis 链接或执行出错'
    },
    '50150': {
        'code': '50150',
        'zh-cn': u'未支持的资源类型'
    }
}
