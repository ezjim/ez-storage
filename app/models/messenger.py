#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'James Iter'
__date__ = '15/7/1'
__contact__ = 'james.iter.cn@gmail.com'
__copyright__ = '(c) 2015 by James Iter.'

from app.initialize import *
from app.models import states
from abc import ABCMeta, abstractmethod
import json

r_push = r


class Message(object):
    __metaclass__ = ABCMeta

    def __init__(self, channel, message):
        self.channel = channel
        self.message = message

    @abstractmethod
    def previewing(self):
        pass

    def send(self):
        args_rules = [
            (int, 'channel', states.MessageChannel.__members__.values()),
            (basestring, 'message')
        ]

        ret = ji.Check.previewing(args_rules, self.__dict__)
        if '200' != ret['state']['code']:
            return ret

        ret = self.previewing()
        if '200' != ret['state']['code']:
            return ret

        try:
            r_push.rpush(es.config['PUSH_QUEUE'], json.dumps(self.__dict__))
        except redis.RedisError, e:
            logger.warning(e.message)
            ret['state'] = ji.Common.exchange_state(50051)
            ret['state']['sub']['zh-cn'] = ''.join([ret['state']['sub']['zh-cn'], '。 详细信息：', e.message])
        except Exception, e:
            logger.error(e.message)
            ret['state'] = ji.Common.exchange_state(50051)
            ret['state']['sub']['zh-cn'] = ''.join([ret['state']['sub']['zh-cn'], '。 详细信息：', e.message])

        return ret


class SMS(Message):
    def __init__(self, mobiles=None, message=''):
        super(SMS, self).__init__(channel=states.MessageChannel.SMS.value, message=message)
        self.mobiles = mobiles
        self.sms_length = message.__len__()

    def previeving(self):
        args_rules = [
            (list, 'mobiles'),
            (int, 'sms_length', (es.config['SMS_LENGTH_MIN'], es.config['SMS_LENGTH_MAX']))
        ]

        for i, mobile in enumerate(self.mobiles):
            self.__dict__['tmp_var_mobile_%s' % i] = mobile
            self.__dict__['tmp_var_mobile_length_%s' % i] = mobile.__len__()
            args_rules.append((basestring, 'tmp_var_mobile_%s' % i))
            args_rules.append((int, 'tmp_var_mobile_length_%s' % i, [es.config['MOBILE_LENGTH']]))

        ret = ji.Check.previewing(args_rules, self.__dict__)
        if '200' != ret['state']['code']:
            return ret

        for i, mobile in enumerate(self.mobiles):
            del self.__dict__['tmp_var_mobile_%s' % i]
            del self.__dict__()['tmp_var_mobile_length_%s' % i]

        return ret


class Email(Message):
    def __init__(self, sender=es.config['DEFAULT_MAIL_SENDER'], receivers=None, title='', message='', others=None):
        super(Email, self).__init__(channel=states.MessageChannel.email.value, message=message)
        self.sender = sender
        self.receivers = receivers
        self.title = title
        self.others = others
        if others is None:
            self.others = {}

    def previeving(self):
        args_rules = [
            (basestring, 'sender'),
            (list, 'receivers'),
            (basestring, 'title'),
            (basestring, 'message'),
            (dict, 'others')
        ]

        for i, receiver in enumerate(self.receivers):
            self.__dict__['tmp_var_receiver_%s' % i] = receiver
            self.__dict__['tmp_var_receiver_length_%s' % i] = receiver.__len__()
            args_rules.append((basestring, 'tmp_var_receiver_%s' % i))
            args_rules.append((int, 'tmp_var_receiver_length_%s' % i, (es.config['EMAIL_STRING_LENGTH_MIN'],
                                                                       es.config['EMAIL_STRING_LENGTH_MAX'])))

        ret = ji.Check.previewing(args_rules, self.__dict__)
        if '200' != ret['state']['code']:
            return ret

        for i, receiver in enumerate(self.receivers):
            del self.__dict__['tmp_var_receiver_%s' % i]
            del self.__dict__['tmp_var_receiver_length_%s' % i]

        return ret
