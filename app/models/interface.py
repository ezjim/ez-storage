#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'James Iter'
__date__ = '15/8/19'
__contact__ = 'james.iter.cn@gmail.com'
__copyright__ = '(c) 2015 by James Iter.'

from abc import ABCMeta, abstractmethod


class CURD(object):
    __metaclass__ = ABCMeta

    def __init__(self):
        pass

    @abstractmethod
    def previewing(self):
        pass

    @abstractmethod
    def create(self):
        pass

    @abstractmethod
    def save(self):
        pass

    @abstractmethod
    def get(self):
        pass

    @abstractmethod
    def update(self):
        pass

    @abstractmethod
    def delete(self):
        pass
