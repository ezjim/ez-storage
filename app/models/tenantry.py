#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'James Iter'
__date__ = '15/6/23'
__contact__ = 'james.iter.cn@gmail.com'
__copyright__ = '(c) 2015 by James Iter.'

import json

from app.initialize import *
from app.models.interface import CURD
from rules import Rules


class Tenantry(CURD):

    def __init__(self, **kwargs):
        self.uid = 0
        self.organization_name = kwargs.get('organization_name', '')
        super(Tenantry, self).__init__()

    def previewing_by_account(self):
        from app.models import Account
        ret = dict()
        ret['state'] = ji.Common.exchange_state(20000)

        account = Account()
        account.uid = self.uid
        if not account.exist():
            ret['state'] = ji.Common.exchange_state(40401)
            ret['state']['sub']['zh-cn'] = ''.join([ret['state']['sub']['zh-cn'], ': uid: ', str(self.uid)])
            raise ji.PreviewingError(json.dumps(ret))

    def previewing(self):
        args_rules = [
            Rules.UID.value,
            Rules.ORGANIZATION_NAME.value
        ]

        ji.Check.previewing(args_rules, self.__dict__)

        self.previewing_by_account()

    def exist(self):
        self.previewing_by_account()
        key_path = ''.join([es.config['TENANTRY_KEY_HEADER'], str(self.uid)])

        return r.exists(key_path)

    def save(self):
        self.previewing()
        # 保存即覆盖，无需检测是否已存在

        key_path = ''.join([es.config['TENANTRY_KEY_HEADER'], str(self.uid)])

        pipe = r.pipeline()
        pipe.hmset(key_path, self.__dict__)
        pipe.execute()

    def create(self, **kwargs):
        ret = dict()
        ret['state'] = ji.Common.exchange_state(20000)

        if self.exist():
            ret['state'] = ji.Common.exchange_state(40901)
            ret['state']['sub']['zh-cn'] = ''.join([ret['state']['sub']['zh-cn'], ': uid: ', str(self.uid)])
            raise ji.PreviewingError(json.dumps(ret))

        self.organization_name = kwargs.get('organization_name', '')
        self.save()

    def update(self, **kwargs):
        ret = dict()
        ret['state'] = ji.Common.exchange_state(20000)

        if not self.exist():
            ret['state'] = ji.Common.exchange_state(40401)
            ret['state']['sub']['zh-cn'] = ''.join([ret['state']['sub']['zh-cn'], ': uid: ', str(self.uid)])
            raise ji.PreviewingError(json.dumps(ret))

        for k, v in kwargs.items():
            if k == 'uid':
                continue

            if hasattr(self, k):
                setattr(self, k, v)

        self.save()

    def get(self):
        ret = dict()
        ret['state'] = ji.Common.exchange_state(20000)

        if not self.exist():
            ret['state'] = ji.Common.exchange_state(40401)
            ret['state']['sub']['zh-cn'] = ''.join([ret['state']['sub']['zh-cn'], ': uid: ', str(self.uid)])
            raise ji.PreviewingError(json.dumps(ret))

        key_path = ''.join([es.config['TENANTRY_KEY_HEADER'], str(self.uid)])
        j_tenantry = r.hgetall(key_path)
        for k, v in j_tenantry.items():
            if hasattr(self, k):
                if k in ['uid']:
                    setattr(self, k, int(v))
                else:
                    setattr(self, k, v)

    def delete(self):
        # TODO: 需要考虑处理已存在的buckets、access_keys问题(方法一: 应用启动时，载入当前系统中已存在的tenantry，每次收到资源请求时，
        # 先判断目标租户是否存在，如果不存在则不予访问(关于应用启动后创建、删除的租户，可以通过一个线程检测消息队列来维护当前的最新tenantry集合))
        ret = dict()
        ret['state'] = ji.Common.exchange_state(20000)

        if not self.exist():
            ret['state'] = ji.Common.exchange_state(40401)
            ret['state']['sub']['zh-cn'] = ''.join([ret['state']['sub']['zh-cn'], ': uid: ', str(self.uid)])
            raise ji.PreviewingError(json.dumps(ret))

        key_path = ''.join([es.config['TENANTRY_KEY_HEADER'], str(self.uid)])

        pipe = r.pipeline()
        pipe.delete(key_path)
        pipe.execute()

    @staticmethod
    def decode_payload(access_key_id=None, payload=None):
        from app.models.access_key import AccessKey
        import jwt

        access_key = AccessKey(id=access_key_id)
        access_key.get()

        ret = dict()
        try:
            return jwt.decode(jwt=payload, key=access_key.secret, algorithms=es.config['JWT_ALGORITHM'],
                                        options=es.config['JWT_OPTIONS'])
        except jwt.InvalidTokenError, e:
            logger.error(e.message)
            ret['state'] = ji.Common.exchange_state(41208)
            raise ji.PreviewingError(json.dumps(ret))
        except Exception, e:
            logger.error(e.message)
            ret['state'] = ji.Common.exchange_state(41208)
            raise ji.PreviewingError(json.dumps(ret))
