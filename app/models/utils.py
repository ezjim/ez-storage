#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'James Iter'
__date__ = '15/6/23'
__contact__ = 'james.iter.cn@gmail.com'
__copyright__ = '(c) 2015 by James Iter.'

import json
import re
from flask import make_response
from flask.wrappers import Response
from functools import wraps
from werkzeug.utils import import_string, cached_property
from app.initialize import *
import states
from rules import Rules


class Utils(object):

    @staticmethod
    def generate_id_by(type_):
        if type_ in (states.IDsField.UID.value, states.IDsField.GID.value):
            # 返回类型为long
            return r.hincrby(es.config['IDsKeeper'], type_, 1)

        elif re.match(''.join(['^', states.IDsField.IndexNodeNumber.value, ':', es.config['BUCKET_NAME_PATTERN'][1:]]),
                      type_) is not None:
            return r.hincrby(es.config['IDsKeeper'], type_, 1)

        else:
            return -1

    @staticmethod
    def delete_id_keeper_by(type_):
        if type_ in (states.IDsField.UID.value, states.IDsField.GID.value):
            return r.hdel(es.config['IDsKeeper'], type_)

        elif re.match(''.join(['^', states.IDsField.IndexNodeNumber.value, ':', es.config['BUCKET_NAME_PATTERN'][1:]]),
                      type_) is not None:
            return r.hdel(es.config['IDsKeeper'], type_)

        else:
            return -1

    @staticmethod
    def get_id_of_max_by(type_):
        if type_ in (states.IDsField.UID.value, states.IDsField.GID.value):
            return long(r.hget(es.config['IDsKeeper'], type_))
        else:
            return -1

    @staticmethod
    def separate_bucket_path_spec(resource_path=None):
        args_rules = [
            Rules.RESOURCE_PATH.value
        ]

        ret = ji.Check.previewing(args_rules, locals())

        spec = None
        offset = resource_path.find('@')
        if offset != -1:
            """ spec_offset 不用+1，即使spec字符串起始为@，spec_format也会处理掉 """
            spec = resource_path[offset:]
            """ 与上面顺序不能颠倒，注意resource_path的赋值 """
            resource_path = resource_path[0:offset]

        resource_path = resource_path.strip('/')
        offset = resource_path.find('/')
        if offset == -1:
            ret['state'] = ji.Common.exchange_state(41251)
            ret['state']['sub']['zh-cn'] = ''.join([ret['state']['sub']['zh-cn'], ': resource_path: ', resource_path,
                                                    '其中至少要包含根/'])
            raise ji.PreviewingError(json.dumps(ret))

        bucket = resource_path[0:offset]
        """ offset 不用+1， 这样资源路径可以携带'/'，看起来更像一个UNIX路径 """
        path = resource_path[offset:]

        return bucket, path, spec

    @staticmethod
    def dumps2response(func):
        """
        视图装饰器
        http://dormousehole.readthedocs.org/en/latest/patterns/viewdecorators.html
        """
        @wraps(func)
        def _dumps2response(*args, **kwargs):
            ret = func(*args, **kwargs)

            if func.func_name != 'r_before_request' and ret is None:
                ret = dict()
                ret['state'] = ji.Common.exchange_state(20000)

            if isinstance(ret, dict) and 'state' in ret:
                response = make_response()
                response.data = json.dumps(ret, ensure_ascii=False)
                response.status_code = int(ret['state']['code'])
                return response

            if isinstance(ret, Response):
                return ret

        return _dumps2response


class LazyView(object):
    """
    惰性载入视图
    http://dormousehole.readthedocs.org/en/latest/patterns/lazyloading.html
    """

    def __init__(self, import_name):
        self.__module__, self.__name__ = import_name.rsplit('.', 1)
        self.import_name = import_name

    @cached_property
    def view(self):
        return import_string(self.import_name)

    def __call__(self, *args, **kwargs):
        return self.view(*args, **kwargs)


def add_rule(blueprint, rule, view_func=None, **options):
    blueprint.add_url_rule(rule=rule, view_func=LazyView(''.join(['app.views.', view_func])), **options)

