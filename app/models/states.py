#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'James Iter'
__date__ = '15/6/23'
__contact__ = 'james.iter.cn@gmail.com'
__copyright__ = '(c) 2015 by James Iter.'

from enum import IntEnum, Enum


class IDsField(Enum):
    UID = 'UID'                             # 账号uid域
    GID = 'GID'                             # 账号gid域
    IndexNodeNumber = 'IndexNodeNumber'     # index node编号


class MessageChannel(IntEnum):
    SMS = 1
    email = 2
    push_ios = 3
    push_android = 4


class BucketPolicy(IntEnum):
    private = 0
    public_read = 1
    public_read_write = 2


class FileType(IntEnum):
    file = 1
    directory = 2
