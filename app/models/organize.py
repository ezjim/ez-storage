#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'James Iter'
__date__ = '15/6/16'
__contact__ = 'james.iter.cn@gmail.com'
__copyright__ = '(c) 2015 by James Iter.'

import json

from interface import CURD
from app.initialize import *
from rules import Rules
from utils import Utils
import states


class IndexNode(CURD):
    """
    https://zh.wikipedia.org/wiki/Inode
    """

    def __init__(self, **kwargs):
        super(IndexNode, self).__init__()
        # 参考: https://zh.wikipedia.org/wiki/Inode#POSIX_inode
        self.bucket = kwargs.get('bucket')
        self.parent_index_node = kwargs.get('parent_index_node')
        self.index_node = kwargs.get('index_node', 0)
        self.file_type = kwargs.get('file_type')
        self.name = kwargs.get('name')              # 不论文件还是目录，都有对人类友好的name，由于redis的特性，这里没必要专门把name抽出来，造成结构更复杂的局面
        self.join_at = 0

    def previewing_by_bucket(self):
        from app.models import Bucket
        bucket = Bucket()
        bucket.name = self.bucket
        # 判断该bucket是否存在，不存在会抛异常
        bucket.get()

    def previewing(self):
        self.previewing_by_bucket()

        args_rules = [
            Rules.INDEX_NODE.value,
            Rules.PARENT_INDEX_NODE.value,
            Rules.FILE_TYPE.value,
        ]

        if self.index_node == 1:
            args_rules.append(Rules.FILE_NAME_BY_ROOT.value)
        else:
            args_rules.append(Rules.FILE_NAME.value)

        ji.Check.previewing(args_rules, self.__dict__)

    def exist(self):
        self.previewing_by_bucket()

        args_rules = [
            Rules.INDEX_NODE.value
        ]
        ji.Check.previewing(args_rules, self.__dict__)

        key_path = ''.join([es.config['INDEX_NODE_METADATA_HEADER'], self.bucket, ':', str(self.index_node)])
        return r.exists(key_path)

    def is_dir(self):
        mapping_key_path = ''.join([es.config['NAME_INDEX_NODE_MAPPING_HEADER'], self.bucket, ':',
                                    str(self.index_node)])
        return r.exists(mapping_key_path)

    def is_file(self):
        key_path = ''.join([es.config['FILE_KEY_HEADER'], str(self.index_node)])
        return r.exists(key_path)

    def parent_exist(self):
        parent_index_node = IndexNode()
        parent_index_node.bucket = self.bucket
        parent_index_node.index_node = self.parent_index_node

        return parent_index_node.exist()

    # 按理exist_in_parent、exist_by_name因置于Directory与File两个子类中，但由于本该在子类中的name属性被提了上来，所以这两个方法也跟着name被提上来
    def exist_in_parent(self):
        # 若判断根是否存在，则直接返回真
        if self.index_node == 1:
            return True

        ret = dict()
        ret['state'] = ji.Common.exchange_state(20000)

        if not self.parent_exist():
            ret['state'] = ji.Common.exchange_state(40401)
            ret['state']['sub']['zh-cn'] = ''.join([ret['state']['sub']['zh-cn'], ': parent index node: ',
                                                    str(self.parent_index_node)])
            raise ji.PreviewingError(json.dumps(ret))

        parent_tree_key_path = ''.join([es.config['INDEX_NODE_TREE_HEADER'], self.bucket, ':',
                                        str(self.parent_index_node)])

        return r.zscore(parent_tree_key_path, self.index_node) is not None

    def exist_by_name(self):
        # 若判断根是否存在，则直接返回真
        if self.name == '/':
            return True

        ret = dict()
        ret['state'] = ji.Common.exchange_state(20000)
        if not self.parent_exist():
            ret['state'] = ji.Common.exchange_state(40401)
            ret['state']['sub']['zh-cn'] = ''.join([ret['state']['sub']['zh-cn'], ': parent index node: ',
                                                    str(self.parent_index_node)])
            raise ji.PreviewingError(json.dumps(ret))

        parent_mapping_key_path = ''.join([es.config['NAME_INDEX_NODE_MAPPING_HEADER'], self.bucket, ':',
                                           str(self.parent_index_node)])

        return r.hexists(parent_mapping_key_path, self.name)

    def get_by_path(self, path=None):
        args_rules = [
            Rules.PATH.value
        ]
        ret = ji.Check.previewing(args_rules, locals())
        # TODO: 对bucket做权限校验，检查是否符合bucket的读写策略
        self.parent_index_node = 1
        path_s = path.strip('/').split('/')

        for i, item in enumerate(path_s):
            self.name = item
            self.get_by_name()

            if (i + 1) != path_s.__len__():
                if self.file_type == states.FileType.file.value:
                    ret['state'] = ji.Common.exchange_state(40401)
                    ret['state']['sub']['zh-cn'] = ''.join([ret['state']['sub']['zh-cn'],
                                                            ': 深度: ', str(i+1), ' --> ', self.name, '，不为目录!'])
                    raise ji.PreviewingError(json.dumps(ret))

                self.parent_index_node = self.index_node

    def save(self):
        self.previewing()
        # 保存即覆盖，无需检测是否已存在

        key_path = ''.join([es.config['INDEX_NODE_METADATA_HEADER'], self.bucket, ':', str(self.index_node)])
        pipe = r.pipeline()
        if not type(self) is IndexNode:
            _index_node = IndexNode()
            for k in _index_node.__dict__.keys():
                _index_node.__dict__[k] = self.__dict__[k]

            pipe.hmset(key_path, _index_node.__dict__)
        else:
            pipe.hmset(key_path, self.__dict__)
        pipe.execute()

    def create(self, root=False):
        self.previewing_by_bucket()

        self.index_node = Utils.generate_id_by(''.join([states.IDsField.IndexNodeNumber.value, ':', self.bucket]))
        self.join_at = ji.Common.tus()

        ret = dict()
        ret['state'] = ji.Common.exchange_state(20000)

        if root:
            if self.index_node != 1:
                ret['state'] = ji.Common.exchange_state(50050)
                ret['state']['sub']['zh-cn'] = ''.join([ret['state']['sub']['zh-cn'], ': 应为1，实际取得: ',
                                                        str(self.index_node)])
                raise ji.PreviewingError(json.dumps(ret))

            self.parent_index_node = self.index_node
            self.name = '/'

        else:
            if not self.parent_exist():
                ret['state'] = ji.Common.exchange_state(40401)
                ret['state']['sub']['zh-cn'] = ''.join([ret['state']['sub']['zh-cn'], ': parent index node: ',
                                                        str(self.parent_index_node)])
                raise ji.PreviewingError(json.dumps(ret))

            if self.exist_by_name():
                ret['state'] = ji.Common.exchange_state(40901)
                ret['state']['sub']['zh-cn'] = ''.join([ret['state']['sub']['zh-cn'], ': file name: ',
                                                        self.name])
                raise ji.PreviewingError(json.dumps(ret))

        if not type(self) is IndexNode:
            super(self.__class__, self).save()
        else:
            self.save()

    def update(self):
        self.save()

    def get(self):
        # TODO: 考虑是否加入真实资源不存在情况下，删除对应的文件组织，这样就不用在resource中delete时，遍历关联的文件组织
        ret = dict()
        ret['state'] = ji.Common.exchange_state(20000)

        if not self.exist():
            ret['state'] = ji.Common.exchange_state(40401)
            ret['state']['sub']['zh-cn'] = ''.join([ret['state']['sub']['zh-cn'], ': index node: ',
                                                    str(self.index_node)])
            raise ji.PreviewingError(json.dumps(ret))

        key_path = ''.join([es.config['INDEX_NODE_METADATA_HEADER'], self.bucket, ':', str(self.index_node)])
        j_data = r.hgetall(key_path)
        _index_node = IndexNode()
        for k, v in j_data.items():
            if hasattr(_index_node, k):
                if k in ['parent_index_node', 'index_node', 'file_type', 'join_at']:
                    setattr(self, k, int(v))
                else:
                    setattr(self, k, v)

    def get_by_name(self):
        ret = dict()
        ret['state'] = ji.Common.exchange_state(20000)

        if not self.exist_by_name():
            ret['state'] = ji.Common.exchange_state(40401)
            ret['state']['sub']['zh-cn'] = ''.join([ret['state']['sub']['zh-cn'], ': name: ',
                                                    str(self.name)])
            raise ji.PreviewingError(json.dumps(ret))

        if self.name == '/':
            self.index_node = 1
        else:
            parent_mapping_key_path = ''.join([es.config['NAME_INDEX_NODE_MAPPING_HEADER'], self.bucket, ':',
                                               str(self.parent_index_node)])

            self.index_node = int(r.hget(parent_mapping_key_path, self.name))

        self.get()

    def delete(self):
        self.previewing()

        ret = dict()
        ret['state'] = ji.Common.exchange_state(20000)

        if not self.exist():
            ret['state'] = ji.Common.exchange_state(40401)
            ret['state']['sub']['zh-cn'] = ''.join([ret['state']['sub']['zh-cn'], ': index node: ',
                                                    str(self.index_node)])
            raise ji.PreviewingError(json.dumps(ret))

        Utils.delete_id_keeper_by(''.join([states.IDsField.IndexNodeNumber.value, ':', self.bucket]))
        key_path = ''.join([es.config['INDEX_NODE_METADATA_HEADER'], self.bucket, ':', str(self.index_node)])
        pipe = r.pipeline()
        pipe.delete(key_path)
        pipe.execute()


class Directory(IndexNode):

    def __init__(self, **kwargs):
        super(Directory, self).__init__(**kwargs)
        self.child = list()
        self.file_type = states.FileType.directory.value
        self.start_position = 0
        self.end_position = 20
        self.count = 0

    def previewing(self):
        args_rules = [
            Rules.DIRECTORY_CHILD.value
        ]

        ji.Check.previewing(args_rules, self.__dict__)
        super(Directory, self).previewing()

    def save(self):
        self.previewing()
        # 保存即覆盖，无需检测是否已存在

        parent_tree_key_path = ''.join([es.config['INDEX_NODE_TREE_HEADER'], self.bucket, ':',
                                        str(self.parent_index_node)])
        parent_mapping_key_path = ''.join([es.config['NAME_INDEX_NODE_MAPPING_HEADER'], self.bucket, ':',
                                           str(self.parent_index_node)])

        mapping_key_path = ''.join([es.config['NAME_INDEX_NODE_MAPPING_HEADER'], self.bucket, ':',
                                    str(self.index_node)])

        pipe = r.pipeline()
        if self.index_node > 1:
            pipe.zadd(parent_tree_key_path, ji.Common.tus(), self.index_node)
            pipe.hset(parent_mapping_key_path, self.name, self.index_node)

        pipe.hset(mapping_key_path, '.', self.index_node)
        pipe.hset(mapping_key_path, '..', self.parent_index_node)
        pipe.execute()

    def create(self, root=False, **kwargs):
        self.bucket = kwargs.get('bucket')

        if not root:
            self.name = kwargs.get('name')
            self.parent_index_node = kwargs.get('parent_index_node')

        super(Directory, self).create(root=root)

        self.save()

    def get(self):
        ret = dict()
        ret['state'] = ji.Common.exchange_state(20000)
        if not super(Directory, self).exist():
            ret['state'] = ji.Common.exchange_state(40401)
            ret['state']['sub']['zh-cn'] = ''.join([ret['state']['sub']['zh-cn'], ': index node: ',
                                                    str(self.index_node)])
            raise ji.PreviewingError(json.dumps(ret))

        super(Directory, self).get()
        if self.index_node > 1 and not self.exist_in_parent():
            ret['state'] = ji.Common.exchange_state(40401)
            ret['state']['sub']['zh-cn'] = ''.join([ret['state']['sub']['zh-cn'], ': 不存在于父目录中, parent index node: ',
                                                    str(self.parent_index_node)])
            raise ji.PreviewingError(json.dumps(ret))

        tree_key_path = ''.join([es.config['INDEX_NODE_TREE_HEADER'], self.bucket, ':',
                                 str(self.index_node)])
        self.count = r.zcard(tree_key_path)
        child_index_node = r.zrange(tree_key_path, start=self.start_position, end=self.end_position)
        for item in child_index_node:
            item = int(item)
            # 如果index node为根自身，则跳过
            if item == 1:
                continue
            index_node = IndexNode()
            index_node.bucket = self.bucket
            index_node.index_node = item
            index_node.get()
            if index_node.file_type == states.FileType.directory.value:
                pass
            elif index_node.file_type == states.FileType.file.value:
                pass
            else:
                logger.warning(''.join(['未知的文件类型: ', str(index_node.file_type)]))
                continue

            self.child.append(index_node.__dict__)

    def update(self):
        self.save()

    def delete(self):
        self.previewing()

        ret = dict()
        ret['state'] = ji.Common.exchange_state(20000)
        # 若为空，根目录中仅存根自身的index node号
        if self.count > 0:
            ret['state'] = ji.Common.exchange_state(41251)
            ret['state']['sub']['zh-cn'] = ''.join([ret['state']['sub']['zh-cn'], '，禁止删除: ',
                                                    str(self.name)])
            raise ji.PreviewingError(json.dumps(ret))

        super(Directory, self).delete()

        parent_tree_key_path = ''.join([es.config['INDEX_NODE_TREE_HEADER'], self.bucket, ':',
                                        str(self.parent_index_node)])
        parent_mapping_key_path = ''.join([es.config['NAME_INDEX_NODE_MAPPING_HEADER'], self.bucket, ':',
                                           str(self.parent_index_node)])

        mapping_key_path = ''.join([es.config['NAME_INDEX_NODE_MAPPING_HEADER'], self.bucket, ':',
                                    str(self.index_node)])

        pipe = r.pipeline()
        pipe.hdel(mapping_key_path, '.')
        pipe.hdel(mapping_key_path, '..')

        pipe.hdel(parent_mapping_key_path, self.name)
        pipe.zrem(parent_tree_key_path, self.index_node)
        pipe.execute()


class File(IndexNode):

    def __init__(self, **kwargs):
        super(File, self).__init__(**kwargs)
        self.resource_id = ''
        self.get_count = 0
        self.uploader_ip = ''
        self.file_type = states.FileType.file.value

    def previewing(self):
        args_rules = [
            Rules.RESOURCE_ID.value,
            Rules.GET_COUNT.value
        ]

        ji.Check.previewing(args_rules, self.__dict__)
        super(File, self).previewing()

    def save(self):
        self.previewing()

        parent_tree_key_path = ''.join([es.config['INDEX_NODE_TREE_HEADER'], self.bucket, ':',
                                        str(self.parent_index_node)])
        parent_mapping_key_path = ''.join([es.config['NAME_INDEX_NODE_MAPPING_HEADER'], self.bucket, ':',
                                           str(self.parent_index_node)])

        key_path = ''.join([es.config['FILE_KEY_HEADER'], str(self.index_node)])

        pipe = r.pipeline()
        pipe.zadd(parent_tree_key_path, ji.Common.tus(), self.index_node)
        pipe.hset(parent_mapping_key_path, self.name, self.index_node)

        for item in ['resource_id', 'get_count']:
            if hasattr(self, item):
                pipe.hset(key_path, item, getattr(self, item))
        pipe.execute()

    def create(self, **kwargs):
        self.bucket = kwargs.get('bucket')
        self.name = kwargs.get('name')
        self.parent_index_node = kwargs.get('parent_index_node')
        self.resource_id = kwargs.get('resource_id')

        if not self.allowed_file():
            ret = dict()
            ret['state'] = ji.Common.exchange_state(40300)
            raise ji.PreviewingError(json.dumps(ret))

        # TODO: 判断real_file_key是否存在
        super(File, self).create()

        self.save()

    def get(self):
        ret = dict()
        ret['state'] = ji.Common.exchange_state(20000)
        if not super(File, self).exist():
            ret['state'] = ji.Common.exchange_state(40401)
            ret['state']['sub']['zh-cn'] = ''.join([ret['state']['sub']['zh-cn'], ': index node: ',
                                                    str(self.index_node)])
            raise ji.PreviewingError(json.dumps(ret))

        super(File, self).get()
        if not self.exist_in_parent():
            ret['state'] = ji.Common.exchange_state(40401)
            ret['state']['sub']['zh-cn'] = ''.join([ret['state']['sub']['zh-cn'], ': 不存在于父目录中, parent index node: ',
                                                    str(self.parent_index_node)])
            raise ji.PreviewingError(json.dumps(ret))

        key_path = ''.join([es.config['FILE_KEY_HEADER'], str(self.index_node)])
        j_data = r.hgetall(key_path)
        for k, v in j_data.items():
            if hasattr(self, k):
                if k in ['get_count']:
                    setattr(self, k, int(v))
                else:
                    setattr(self, k, v)

    def update(self):
        self.save()

    def delete(self):
        self.previewing()

        super(File, self).delete()

        parent_tree_key_path = ''.join([es.config['INDEX_NODE_TREE_HEADER'], self.bucket, ':',
                                        str(self.parent_index_node)])
        parent_mapping_key_path = ''.join([es.config['NAME_INDEX_NODE_MAPPING_HEADER'], self.bucket, ':',
                                           str(self.parent_index_node)])

        key_path = ''.join([es.config['FILE_KEY_HEADER'], str(self.index_node)])
        pipe = r.pipeline()
        pipe.hdel(parent_mapping_key_path, self.name)
        pipe.zrem(parent_tree_key_path, self.index_node)

        pipe.delete(key_path)
        pipe.execute()

    def allowed_file(self):
        return '.' in self.name and \
               self.name.rsplit('.', 1)[1].lower() in es.config['ALLOWED_EXTENSIONS']

