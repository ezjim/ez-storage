#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'James Iter'
__date__ = '15/8/22'
__contact__ = 'james.iter.cn@gmail.com'
__copyright__ = '(c) 2015 by James Iter.'


import json
from flask import make_response
import jwt

from app.initialize import *
from utils import Utils
import states
from interface import CURD
from rules import Rules
from account import Account


class AccessControl(object):

    def __init__(self):
        self.account = None
        self.token = None
        self.endpoint = None

    def previewing(self):
        ret = dict()
        ret['state'] = ji.Common.exchange_state(20000)
        if self.account is None:
            ret['state'] = ji.Common.exchange_state(40101)
            raise ji.PreviewingError(json.dumps(ret))

        if not self.account.exist():
            ret['state'] = ji.Common.exchange_state(40401)
            ret['state']['sub']['zh-cn'] = ''.join([ret['state']['sub']['zh-cn'], ': ', self.account.uid])
            raise ji.PreviewingError(json.dumps(ret))

    def identity_authentication(self, **kwargs):
        """
        身份验证(通过账号+密码验证身份)
        :return:
        """
        args_rules = [
            Rules.LOGIN_NAME.value,
            Rules.PASSWORD.value
        ]

        ji.Check.previewing(args_rules, kwargs)

        self.account = Account()
        self.account.login_name = kwargs['login_name']

        self.account.get_by_login_name()
        return ji.Security.ji_pbkdf2_check(password=kwargs['password'], password_hash=self.account.password)

    def permission_check(self):
        """
        权限校验
        :return:
        """
        ret = dict()
        ret['state'] = ji.Common.exchange_state(20000)

        self.previewing()
        privileges = RoleGroup.get_privileges_by_uid(uid=self.account.uid)

        if self.endpoint not in ('static', None) and \
                not self.is_not_need_to_auth() and \
                self.endpoint not in privileges and \
                self.account.uid != 1:                          # admin(管理员无需判断权限)的uid
            ret['state'] = ji.Common.exchange_state(40301)
            raise ji.PreviewingError(json.dumps(ret))

    def login(self, **kwargs):
        """
        登陆处理
        :return:
        """
        args_rules = [
            Rules.LOGIN_NAME.value,
            Rules.PASSWORD.value
        ]

        ret = ji.Check.previewing(args_rules, kwargs)

        if not self.identity_authentication(**kwargs):
            ret['state'] = ji.Common.exchange_state(40101)
            raise ji.PreviewingError(json.dumps(ret))

        self.get_token()
        rep = make_response()
        rep.set_cookie('token', self.token)
        rep.data = json.dumps({'state': ji.Common.exchange_state(20000)}, ensure_ascii=False)
        return rep

    @staticmethod
    def logout():
        rep = make_response()
        rep.delete_cookie('token')
        return rep

    def generate_token(self):
        self.previewing()

        key_path = ''.join([es.config['TOKEN_KEY_HEADER'], str(self.account.uid)])
        payload = {
            'iat': ji.Common.ts(),                                                  # 创建于
            'nbf': ji.Common.ts(),                                                  # 在此之前不可用
            'exp': ji.Common.ts() + es.config['TOKEN_TTL'],                         # 过期时间
            'uid': self.account.uid
        }
        self.token = jwt.encode(payload=payload, key=es.config['JWT_SECRET'], algorithm=es.config['JWT_ALGORITHM'])
        r.setex(key_path, es.config['TOKEN_TTL'], self.token)

    def get_token(self):
        self.previewing()

        key_path = ''.join([es.config['TOKEN_KEY_HEADER'], str(self.account.uid)])
        if r.ttl(key_path) > es.config['TOKEN_AUTO_RENEW_TIME_LEFT']:
            self.token = r.get(key_path)
        else:
            self.generate_token()

    def verify_token(self):
        ret = dict()
        ret['state'] = ji.Common.exchange_state(20000)
        try:
            payload = jwt.decode(jwt=self.token, key=es.config['JWT_SECRET'], algorithms=es.config['JWT_ALGORITHM'],
                                 options=es.config['JWT_OPTIONS'])
        except jwt.InvalidTokenError, e:
            logger.error(e.message)
            ret['state'] = ji.Common.exchange_state(41208)
            # TODO: ret 中应该给予重定向到主页？或者重新申请页？  应该在调用它的父方法根据返回状态码做？
            raise ji.JITError(json.dumps(ret))
        except Exception, e:
            logger.error(e.message)
            raise Exception(e.message)
        self.account = Account()
        self.account.uid = payload['uid']
        self.account.get()

    def is_not_need_to_auth(self):
        not_auth_table = [
            'db.r_clear_db',
            'account.r_account_register',
            'rbac.r_login',
            'resource.r_resources_get'
        ]
        if self.endpoint in not_auth_table:
            return True
        return False


class RoleGroup(CURD):
    """
    角色的用户组
    """

    def __init__(self, **kwargs):
        self.gid = kwargs.get('gid', 0)                     # 组id
        self.name = kwargs.get('name', '')                  # 组名
        self.members = kwargs.get('members', None)          # 组员集合
        self.privileges = kwargs.get('privileges', None)    # 权限集合，表现为视图方法集合
        super(RoleGroup, self).__init__()

    def previewing(self):
        args_rules = [
            Rules.GID.value,
            Rules.ROLE_GROUP_NAME.value,
            Rules.ROLE_GROUP_MEMBERS.value,
            Rules.ROLE_GROUP_PRIVILEGES.value
        ]

        ji.Check.previewing(args_rules, self.__dict__)

        args_rules = list()

        for i, member in enumerate(self.members):
            locals()['tmp_var_member_%s' % i] = member
            args_rules.append(((int, long), 'tmp_var_member_%s' % i))

        for i, privilege in enumerate(self.privileges):
            locals()['tmp_var_privilege_%s' % i] = privilege
            args_rules.append((basestring, 'tmp_var_privilege_%s' % i))

        ji.Check.previewing(args_rules, locals())

        ret = dict()

        account = Account()
        for i, member in enumerate(self.members):
            account.uid = member
            if not account.exist():
                ret['state'] = ji.Common.exchange_state(40401)
                ret['state']['sub']['zh-cn'] = ''.join([ret['state']['sub']['zh-cn'], ': uid: ', str(member)])
                raise ji.PreviewingError(json.dumps(ret))

            del locals()['tmp_var_member_%s' % i]

        privilege = Privilege()
        for i, _privilege in enumerate(self.privileges):
            privilege.privilege = _privilege
            if not privilege.exist():
                ret['state'] = ji.Common.exchange_state(40401)
                ret['state']['sub']['zh-cn'] = ''.join([ret['state']['sub']['zh-cn'], ': privilege: ',
                                                        str(_privilege)])
                raise ji.PreviewingError(json.dumps(ret))

            del locals()['tmp_var_privilege_%s' % i]

    def exist(self):
        args_rules = [
            Rules.GID.value
        ]

        ji.Check.previewing(args_rules, self.__dict__)

        key_path = ''.join([es.config['ROLE_GROUP_KEY_HEADER'], str(self.gid)])
        if not r.exists(key_path):
            return False

        return True

    def exist_by_role_group_name(self):
        args_rules = [
            Rules.ROLE_GROUP_NAME.value
        ]

        ji.Check.previewing(args_rules, self.__dict__)

        if not r.hexists(es.config['ROLE_GROUP_NAME_GID_MAPPING_KEY'], self.name):
            return False

        self.gid = int(r.hget(es.config['ROLE_GROUP_NAME_GID_MAPPING_KEY'], self.name))

        return self.exist()

    def save(self):
        self.previewing()
        # 保存即覆盖，无需检测是否已存在

        key_path = ''.join([es.config['ROLE_GROUP_KEY_HEADER'], str(self.gid)])

        pipe = r.pipeline()
        pipe.hset(es.config['ROLE_GROUP_NAME_GID_MAPPING_KEY'], self.name, self.gid)
        pipe.hmset(key_path, {'name': self.name, 'gid': self.gid})
        pipe.sadd(''.join([es.config['ROLE_GROUP_UID_MAPPING_KEY'], str(self.gid)]), *set(self.members))
        for member in self.members:
            key_path_uid_m_gid = ''.join([es.config['UID_ROLE_GROUP_MAPPING_KEY'], str(member)])
            if not r.sismember(key_path_uid_m_gid, self.gid):
                pipe.sadd(key_path_uid_m_gid, self.gid)

        pipe.sadd(''.join([es.config['ROLE_GROUP_PRIVILEGES_MAPPING_KEY'], str(self.gid)]), *set(self.privileges))
        pipe.execute()

    def create(self, **kwargs):
        args_rules = [
            Rules.ROLE_GROUP_NAME.value,
            Rules.ROLE_GROUP_MEMBERS.value,
            Rules.ROLE_GROUP_PRIVILEGES.value
        ]

        ji.Check.previewing(args_rules, kwargs)

        self.gid = Utils.generate_id_by(states.IDsField.GID.value)
        # 无需判断是否已存在同名的组，即允许两个名字相同的组出现，唯一性根据gid来判别
        self.name = kwargs['name']
        self.members = kwargs['members']
        self.privileges = kwargs['privileges']

        self.save()

    def get(self):
        ret = dict()
        ret['state'] = ji.Common.exchange_state(20000)

        if not self.exist():
            ret['state'] = ji.Common.exchange_state(40401)
            ret['state']['sub']['zh-cn'] = ''.join([ret['state']['sub']['zh-cn'], ': gid: ', str(self.gid)])
            raise ji.PreviewingError(json.dumps(ret))

        key_path = ''.join([es.config['ROLE_GROUP_KEY_HEADER'], str(self.gid)])

        role_group = r.hgetall(key_path)
        for key in self.__dict__.keys():
            if key in role_group:
                if key in ['gid']:
                    self.__setattr__(key, int(role_group[key]))
                else:
                    self.__setattr__(key, role_group[key])

        self.members = []
        self.privileges = []

        for item in r.smembers(''.join([es.config['ROLE_GROUP_UID_MAPPING_KEY'], str(self.gid)])):
            self.members.append(int(item))

        for item in r.smembers(''.join([es.config['ROLE_GROUP_PRIVILEGES_MAPPING_KEY'], str(self.gid)])):
            self.privileges.append(item)

    def get_by_role_group_name(self):
        ret = dict()
        ret['state'] = ji.Common.exchange_state(20000)

        if not self.exist_by_role_group_name():
            ret['state'] = ji.Common.exchange_state(40401)
            ret['state']['sub']['zh-cn'] = ''.join([ret['state']['sub']['zh-cn'], ': role_group_name: ', self.name])
            raise ji.PreviewingError(json.dumps(ret))

        self.get()  # self.gid 已通过上面的 self.exist_by_role_group_name()已获取到

    def update(self):
        self.previewing()

        ret = dict()
        ret['state'] = ji.Common.exchange_state(20000)

        # 先直接删除，然后以创建为更新
        will_delete_role_group = RoleGroup()
        will_delete_role_group.gid = self.gid
        will_delete_role_group.get()
        will_delete_role_group.delete()

        self.save()

    def delete(self):
        self.previewing()

        ret = dict()
        ret['state'] = ji.Common.exchange_state(20000)

        if not self.exist_by_role_group_name():
            ret['state'] = ji.Common.exchange_state(40401)
            ret['state']['sub']['zh-cn'] = ''.join([ret['state']['sub']['zh-cn'], ': role_group_name: ', self.name])
            raise ji.PreviewingError(json.dumps(ret))

        key_path = ''.join([es.config['ROLE_GROUP_KEY_HEADER'], str(self.gid)])

        pipe = r.pipeline()
        pipe.hdel(es.config['ROLE_GROUP_NAME_GID_MAPPING_KEY'], self.name)
        pipe.delete(key_path)
        pipe.delete(''.join([es.config['ROLE_GROUP_UID_MAPPING_KEY'], str(self.gid)]))
        for member in self.members:
            key_path_uid_m_gid = ''.join([es.config['UID_ROLE_GROUP_MAPPING_KEY'], str(member)])
            if r.sismember(key_path_uid_m_gid, self.gid):
                pipe.srem(key_path_uid_m_gid, self.gid)

        pipe.delete(''.join([es.config['ROLE_GROUP_PRIVILEGES_MAPPING_KEY'], str(self.gid)]))
        pipe.execute()

    @staticmethod
    def get_all():
        ret = dict()
        ret['state'] = ji.Common.exchange_state(20000)
        ret['list'] = list()

        # 若 key 不存在，返回空列表 -> http://redisdoc.com/hash/hgetall.html
        name_gid_s = r.hgetall(es.config['ROLE_GROUP_NAME_GID_MAPPING_KEY'])
        for k, v in name_gid_s.items():
            role_group = RoleGroup()
            role_group.gid = int(v)
            role_group.get()

            ret['list'].append(role_group.__dict__)

        return ret

    @staticmethod
    def get_gid_s_by_uid(uid=0):
        ret = dict()
        ret['state'] = ji.Common.exchange_state(20000)

        account = Account()
        account.uid = uid
        if not account.exist():
            ret['state'] = ji.Common.exchange_state(40401)
            ret['state']['sub']['zh-cn'] = ''.join([ret['state']['sub']['zh-cn'], ': uid: ', str(uid)])
            raise ji.PreviewingError(json.dumps(ret))

        key_path_uid_m_gid = ''.join([es.config['UID_ROLE_GROUP_MAPPING_KEY'], str(uid)])
        # 不存在的 key 被视为空集合  ->  http://redisdoc.com/set/smembers.html
        return map(lambda gid: int(gid), r.smembers(key_path_uid_m_gid))

    @staticmethod
    def get_privileges_by_uid(uid=0):
        gid_s = RoleGroup.get_gid_s_by_uid(uid=uid)
        privileges = []
        for gid in gid_s:
            role_group = RoleGroup()
            role_group.gid = gid
            role_group.get()
            if isinstance(role_group.privileges, list):
                privileges.extend(role_group.privileges)

        return privileges


class Privilege(CURD):
    """
    权限
    """

    def __init__(self, privilege='', label=''):
        self.privilege = privilege
        self.label = label
        super(Privilege, self).__init__()

    def previewing(self):
        args_rules = [
            Rules.PRIVILEGE.value,
            Rules.PRIVILEGE_LABEL.value
        ]

        ji.Check.previewing(args_rules, self.__dict__)

        ret = dict()
        ret['state'] = ji.Common.exchange_state(20000)
        if self.privilege not in es.view_functions.keys():
            ret['state'] = ji.Common.exchange_state(40401)
            ret['state']['sub']['zh-cn'] = ''.join([ret['state']['sub']['zh-cn'], ': view_functions: ',
                                                    str(self.privilege)])
            raise ji.PreviewingError(json.dumps(ret))

    def exist(self):
        args_rules = [
            Rules.PRIVILEGE.value
        ]

        ji.Check.previewing(args_rules, self.__dict__)

        if not r.hexists(es.config['PRIVILEGE_LABEL_MAPPING_KEY'], self.privilege):
            return False

        return True

    def exist_by_privilege_label(self):
        args_rules = [
            Rules.PRIVILEGE_LABEL.value
        ]

        ji.Check.previewing(args_rules, self.__dict__)

        if not r.hexists(es.config['LABEL_PRIVILEGE_MAPPING_KEY'], self.label):
            return False

        self.privilege = r.hget(es.config['LABEL_PRIVILEGE_MAPPING_KEY'], self.label)

        return self.exist()

    def save(self):
        self.previewing()
        # 保存即覆盖，无需检测是否已存在

        pipe = r.pipeline()
        pipe.hset(es.config['PRIVILEGE_LABEL_MAPPING_KEY'], self.privilege, self.label)
        pipe.hset(es.config['LABEL_PRIVILEGE_MAPPING_KEY'], self.label, self.privilege)
        pipe.execute()

    def create(self, **kwargs):
        args_rules = [
            Rules.PRIVILEGE.value,
            Rules.PRIVILEGE_LABEL.value
        ]

        ji.Check.previewing(args_rules, kwargs)

        ret = dict()
        ret['state'] = ji.Common.exchange_state(20000)
        self.privilege = kwargs['privilege']
        self.label = kwargs['label']
        # 避免label重名，那样用户无法判别，系统也会混乱
        if self.exist_by_privilege_label():
            ret['state'] = ji.Common.exchange_state(40901)
            ret['state']['sub']['zh-cn'] = ''.join([ret['state']['sub']['zh-cn'], ': label: ', self.label])
            raise ji.PreviewingError(json.dumps(ret))

        self.save()

    def get(self):
        ret = dict()
        ret['state'] = ji.Common.exchange_state(20000)

        if not self.exist():
            ret['state'] = ji.Common.exchange_state(40401)
            ret['state']['sub']['zh-cn'] = ''.join([ret['state']['sub']['zh-cn'], ': privilege: ', str(self.privilege)])
            raise ji.PreviewingError(json.dumps(ret))

        self.label = r.hget(es.config['PRIVILEGE_LABEL_MAPPING_KEY'], self.privilege)

    def get_by_label(self):
        ret = dict()
        ret['state'] = ji.Common.exchange_state(20000)

        if not self.exist_by_privilege_label():
            ret['state'] = ji.Common.exchange_state(40401)
            ret['state']['sub']['zh-cn'] = ''.join([ret['state']['sub']['zh-cn'], ': label: ', self.label])
            raise ji.PreviewingError(json.dumps(ret))

        self.get()  # self.label 已通过上面的 self.exist_by_privilege_label()已获取到

    def update(self):
        self.save()

    def delete(self):
        self.previewing()

        ret = dict()
        ret['state'] = ji.Common.exchange_state(20000)

        if not self.exist_by_privilege_label():
            ret['state'] = ji.Common.exchange_state(40401)
            ret['state']['sub']['zh-cn'] = ''.join([ret['state']['sub']['zh-cn'], ': label: ', self.label])
            raise ji.PreviewingError(json.dumps(ret))

        pipe = r.pipeline()
        pipe.hdel(es.config['PRIVILEGE_LABEL_MAPPING_KEY'], self.privilege)
        pipe.hdel(es.config['LABEL_PRIVILEGE_MAPPING_KEY'], self.label)
        pipe.execute()

    @staticmethod
    def get_all():
        ret = dict()
        ret['state'] = ji.Common.exchange_state(20000)
        ret['list'] = list()
        view_functions = es.view_functions.keys()
        privilege_label = r.hgetall(es.config['PRIVILEGE_LABEL_MAPPING_KEY'])

        for item in view_functions:
            d_item = {'privilege': item, 'label': None}
            if item in privilege_label:
                d_item['label'] = privilege_label[item]

            ret['list'].append(d_item)

        return ret
