#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'James Iter'
__date__ = '15/8/19'
__contact__ = 'james.iter.cn@gmail.com'
__copyright__ = '(c) 2015 by James Iter.'

import json
import jwt

from interface import CURD
from app.initialize import *
from utils import Utils
from rules import Rules
import states


class Account(CURD):

    def __init__(self, **kwargs):
        self.uid = 0
        self.login_name = kwargs.get('login_name', None)
        self.password = kwargs.get('password', None)
        self.join_at = ji.Common.tus()
        self.last_update_at = ji.Common.tus()
        self.mobile_phone = ''
        self.email = ''
        self.mobile_phone_verified = False
        self.email_verified = False
        super(Account, self).__init__()

    def previewing(self):
        args_rules = [
            Rules.UID.value,
            Rules.LOGIN_NAME.value,
            Rules.PASSWORD_HASH.value,
            eval(Rules.JOIN_AT.value)
        ]

        tmp_arg_rules = eval(Rules.LAST_UPDATE_AT.value)
        tmp_arg_rules = (tmp_arg_rules[0], tmp_arg_rules[1], (self.join_at, tmp_arg_rules[2][1]))

        args_rules.append(tmp_arg_rules)

        ji.Check.previewing(args_rules, self.__dict__)

    def exist(self):
        args_rules = [
            Rules.UID.value
        ]

        ji.Check.previewing(args_rules, self.__dict__)

        key_path = ''.join([es.config['ACCOUNT_KEY_HEADER'], str(self.uid)])
        if not r.exists(key_path):
            return False

        return True

    def exist_by_login_name(self):
        args_rules = [
            Rules.LOGIN_NAME.value
        ]

        ji.Check.previewing(args_rules, self.__dict__)

        if not r.hexists(es.config['LOGIN_NAME_UID_MAPPING_KEY'], self.login_name):
            return False

        self.uid = int(r.hget(es.config['LOGIN_NAME_UID_MAPPING_KEY'], self.login_name))

        return self.exist()

    def save(self):
        self.previewing()

        ret = dict()
        ret['state'] = ji.Common.exchange_state(20000)

        if self.exist_by_login_name():
            ret['state'] = ji.Common.exchange_state(40901)
            ret['state']['sub']['zh-cn'] = ''.join([ret['state']['sub']['zh-cn'], ': ', self.login_name])
            raise ji.PreviewingError(json.dumps(ret))

        key_path = ''.join([es.config['ACCOUNT_KEY_HEADER'], str(self.uid)])

        pipe = r.pipeline()
        pipe.hset(es.config['LOGIN_NAME_UID_MAPPING_KEY'], self.login_name, self.uid)
        pipe.hmset(key_path, self.__dict__)
        pipe.execute()

    # TODO: 创建账户时使用
    def generate_invitation_tenantry_url(email):
        # TODO: 复制过来的老代码，需做调整
        payload = {
            'iat': ji.Common.ts(),                                                  # 创建于
            'nbf': ji.Common.ts(),                                                  # 在此之前不可用
            'exp': ji.Common.ts() + es.config['INVITATION_TENANTRY_TOKEN_TTL'],     # 过期时间
            'aud': es.config['JWT_AUDIENCE_INVITATION_TENANTRY'],                   # 受众
            'email': email,
        }
        token = jwt.encode(payload=payload, key=es.config['JWT_SECRET'], algorithm=es.config['JWT_ALGORITHM'])
        return ''.join([es.config['BASES_URL'], '/tenantry/verify/', token])

    # TODO: 创建账户时使用
    def verify(token):
        # TODO: 复制过来的老代码，需做调整
        ret = dict()
        ret['state'] = ji.Common.exchange_state(20000)
        try:
            payload = jwt.decode(jwt=token, key=es.config['JWT_SECRET'], algorithms=es.config['JWT_ALGORITHM'],
                                 options=es.config['JWT_OPTIONS'],
                                 audience=es.config['JWT_AUDIENCE_INVITATION_TENANTRY'])
        except jwt.InvalidTokenError, e:
            logger.error(e.message)
            ret['state'] = ji.Common.exchange_state(41208)
            # TODO: ret 中应该给予重定向到主页？或者重新申请页？  应该在调用它的父方法根据返回状态码做？
            return ret
        except Exception, e:
            logger.error(e.message)
            raise Exception(e.message)

        token_key_path = ''.join([es.config['INVITATION_TENANTRY_KEY_HEADER'], ':', payload['email']])
        if not r.exists(token_key_path):
            logger.error(''.join(['未在数据库中找到对应的token存根: ', token_key_path, ' --> ', token]))
            ret['state'] = ji.Common.exchange_state(41208)
            return ret

        uid = r.hget(es.config['TENANTRY_LOGIN_NAME_UID_MAPPING_KEY'], payload['email'])
        key_path = ''.join([es.config['TENANTRY_KEY_HEADER'], str(uid)])
        r.hset(key_path, 'email_verified', True)

        return ret

    def create(self, **kwargs):
        args_rules = [
            Rules.LOGIN_NAME.value,
            Rules.PASSWORD.value
        ]

        ret = ji.Check.previewing(args_rules, kwargs)

        self.login_name = kwargs['login_name']
        if self.exist_by_login_name():
            ret['state'] = ji.Common.exchange_state(40901)
            ret['state']['sub']['zh-cn'] = ''.join([ret['state']['sub']['zh-cn'], ': ', self.login_name])
            raise ji.PreviewingError(json.dumps(ret))

        self.uid = Utils.generate_id_by(states.IDsField.UID.value)
        self.password = ji.Security.ji_pbkdf2(password=kwargs['password'])
        self.save()

    def get(self):
        ret = dict()
        ret['state'] = ji.Common.exchange_state(20000)

        if not self.exist():
            ret['state'] = ji.Common.exchange_state(40401)
            ret['state']['sub']['zh-cn'] = ''.join([ret['state']['sub']['zh-cn'], ': uid: ', str(self.uid)])
            raise ji.PreviewingError(json.dumps(ret))

        key_path = ''.join([es.config['ACCOUNT_KEY_HEADER'], str(self.uid)])

        account = r.hgetall(key_path)
        for key in self.__dict__.keys():
            if key in account:
                if key in ['uid', 'join_at']:
                    self.__setattr__(key, int(account[key]))
                else:
                    self.__setattr__(key, account[key])

    def update(self):
        self.save()

    def delete(self):
        self.previewing()

        ret = dict()
        ret['state'] = ji.Common.exchange_state(20000)

        if not self.exist_by_login_name():
            ret['state'] = ji.Common.exchange_state(40401)
            ret['state']['sub']['zh-cn'] = ''.join([ret['state']['sub']['zh-cn'], ': ', self.login_name])
            raise ji.PreviewingError(json.dumps(ret))

        key_path = ''.join([es.config['ACCOUNT_KEY_HEADER'], str(self.uid)])

        pipe = r.pipeline()
        pipe.hdel(es.config['LOGIN_NAME_UID_MAPPING_KEY'], self.login_name)
        pipe.delete(key_path)
        pipe.execute()

    def get_by_login_name(self):
        ret = dict()
        ret['state'] = ji.Common.exchange_state(20000)

        if not self.exist_by_login_name():
            ret['state'] = ji.Common.exchange_state(40401)
            ret['state']['sub']['zh-cn'] = ''.join([ret['state']['sub']['zh-cn'], ': ', self.login_name])
            raise ji.PreviewingError(json.dumps(ret))

        self.get()  # self.uid 已通过上面的 self.exist_by_login_name()已获取到

    @staticmethod
    def get_all():
        ret = dict()
        ret['state'] = ji.Common.exchange_state(20000)
        ret['list'] = list()

        max_uid = Utils.get_id_of_max_by(states.IDsField.UID.value)
        """
        注意xrange为 半开半闭区间[a,b)
        http://baike.baidu.com/view/4957124.htm
        https://zh.wikipedia.org/wiki/%E5%8D%80%E9%96%93
        """
        for item in xrange(1, max_uid+1):
            account = Account()
            account.uid = item
            account.get()
            j_account = account.__dict__
            del j_account['password']
            ret['list'].append(j_account)

        return ret
