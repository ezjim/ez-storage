#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'James Iter'
__date__ = '15/8/16'
__contact__ = 'james.iter.cn@gmail.com'
__copyright__ = '(c) 2015 by James Iter.'

import json

from app.initialize import *
from rules import Rules
from interface import CURD


class AccessKey(CURD):

    def __init__(self, **kwargs):
        self.uid = kwargs.get('uid', 0)
        self.id = kwargs.get('id', '')
        self.secret = kwargs.get('secret', '')
        self.join_at = 0
        super(AccessKey, self).__init__()

    def previewing_by_tenantry(self):
        from app.models import Tenantry
        ret = dict()
        ret['state'] = ji.Common.exchange_state(20000)

        tenantry = Tenantry()
        tenantry.uid = self.uid
        if not tenantry.exist():
            ret['state'] = ji.Common.exchange_state(40401)
            ret['state']['sub']['zh-cn'] = ''.join([ret['state']['sub']['zh-cn'], ': 租户(uid): ', str(self.uid)])
            raise ji.PreviewingError(json.dumps(ret))

    def previewing(self):
        args_rules = [
            Rules.UID.value,
            Rules.ACCESS_KEY_ID.value,
            Rules.ACCESS_KEY_SECRET.value
        ]

        ji.Check.previewing(args_rules, self.__dict__)

        self.previewing_by_tenantry()

    def exist_by_key_id(self):
        args_rules = [
            Rules.ACCESS_KEY_ID.value
        ]

        ji.Check.previewing(args_rules, self.__dict__)

        key_path = ''.join([es.config['ACCESS_KEY_HEADER'], self.id])
        return r.sismember(es.config['ACCESS_KEY_SET'], self.id) and r.exists(key_path)

    def save(self):
        self.previewing()
        # 保存即覆盖，无需检测是否已存在

        key_path = ''.join([es.config['ACCESS_KEY_HEADER'], self.id])
        key_path_by_set = ''.join([es.config['ACCESS_KEY_SET_BY_UID_KEY_HEADER'], str(self.uid)])

        pipe = r.pipeline()
        pipe.sadd(es.config['ACCESS_KEY_SET'], self.id)
        pipe.sadd(key_path_by_set, self.id)
        pipe.hmset(key_path, self.__dict__)
        pipe.execute()

    def create(self):
        ret = dict()
        ret['state'] = ji.Common.exchange_state(20000)

        self.id = ji.Common.generate_random_code(length=20)

        while self.exist_by_key_id():
            self.id = ji.Common.generate_random_code(length=20)

        self.secret = ji.Common.generate_random_code(length=40)
        self.join_at = ji.Common.tus()
        self.save()

    def update(self, **kwargs):
        self.previewing_by_tenantry()

        ret = dict()
        ret['state'] = ji.Common.exchange_state(20000)

        self.get()
        for k, v in kwargs.items():
            if k in ['uid', 'id', 'secret', 'join_at']:
                continue

            if hasattr(self, k):
                setattr(self, k, v)

        self.save()

    def get_by_key_id(self):
        ret = dict()
        ret['state'] = ji.Common.exchange_state(20000)

        if not self.exist_by_key_id():
            ret['state'] = ji.Common.exchange_state(40401)
            ret['state']['sub']['zh-cn'] = ''.join([ret['state']['sub']['zh-cn'], ': access key id: ', self.id])
            raise ji.PreviewingError(json.dumps(ret))

        key_path = ''.join([es.config['ACCESS_KEY_HEADER'], self.id])
        j_access_key = r.hgetall(key_path)
        for k, v in j_access_key.items():
            if hasattr(self, k):
                if k in ['uid', 'join_at']:
                    setattr(self, k, int(v))
                else:
                    setattr(self, k, v)

    def get(self):
        self.get_by_key_id()

    def get_by_uid(self):
        self.previewing_by_tenantry()
        key_path_by_set = ''.join([es.config['ACCESS_KEY_SET_BY_UID_KEY_HEADER'], str(self.uid)])

        ret = dict()
        ret['state'] = ji.Common.exchange_state(20000)
        ret['list'] = list()

        for access_key_id in r.smembers(key_path_by_set):
            access_key = AccessKey(id=access_key_id)
            access_key.get_by_key_id()
            ret['list'].append(access_key.__dict__)

        return ret

    def delete(self):
        self.previewing()

        ret = dict()
        ret['state'] = ji.Common.exchange_state(20000)

        if not self.exist_by_key_id():
            ret['state'] = ji.Common.exchange_state(40401)
            ret['state']['sub']['zh-cn'] = ''.join([ret['state']['sub']['zh-cn'], ': access key id: ', self.id])
            raise ji.PreviewingError(json.dumps(ret))

        key_path = ''.join([es.config['ACCESS_KEY_HEADER'], self.id])
        key_path_by_set = ''.join([es.config['ACCESS_KEY_SET_BY_UID_KEY_HEADER'], str(self.uid)])

        pipe = r.pipeline()
        pipe.srem(es.config['ACCESS_KEY_SET'], self.id)
        pipe.srem(key_path_by_set, self.id)
        pipe.delete(key_path)
        pipe.execute()

    @staticmethod
    def get_all():
        ret = dict()
        ret['state'] = ji.Common.exchange_state(20000)
        ret['list'] = list()

        for access_key_id in r.smembers(es.config['ACCESS_KEY_SET']):
            access_key = AccessKey(id=access_key_id)
            access_key.get_by_key_id()
            ret['list'].append(access_key.__dict__)

        return ret
