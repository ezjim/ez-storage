#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'James Iter'
__date__ = '15/8/16'
__contact__ = 'james.iter.cn@gmail.com'
__copyright__ = '(c) 2015 by James Iter.'

import json

from interface import CURD
from rules import Rules
from app.initialize import *
import states


class Bucket(CURD):

    def __init__(self, **kwargs):
        self.uid = kwargs.get('uid', 0)
        self.name = kwargs.get('name', '')
        self.policy = kwargs.get('policy', states.BucketPolicy.private.value)
        self.join_at = 0
        super(Bucket, self).__init__()

    def previewing_by_tenantry(self):
        from app.models import Tenantry
        ret = dict()
        ret['state'] = ji.Common.exchange_state(20000)

        tenantry = Tenantry()
        tenantry.uid = self.uid
        if not tenantry.exist():
            ret['state'] = ji.Common.exchange_state(40401)
            ret['state']['sub']['zh-cn'] = ''.join([ret['state']['sub']['zh-cn'], ': 租户(uid): ', str(self.uid)])
            raise ji.PreviewingError(json.dumps(ret))

    def previewing(self):
        args_rules = [
            Rules.UID.value,
            Rules.BUCKET_NAME.value,
            Rules.BUCKET_POLICY.value
        ]

        ji.Check.previewing(args_rules, self.__dict__)

        self.previewing_by_tenantry()

    def exist_by_name(self):
        args_rules = [
            Rules.BUCKET_NAME.value
        ]

        ji.Check.previewing(args_rules, self.__dict__)

        key_path = ''.join([es.config['BUCKET_KEY_HEADER'], self.name])
        return r.sismember(es.config['BUCKET_SET'], self.name) and r.exists(key_path)

    def save(self):
        self.previewing()
        # 保存即覆盖，无需检测是否已存在

        key_path = ''.join([es.config['BUCKET_KEY_HEADER'], self.name])
        key_path_by_set = ''.join([es.config['BUCKET_SET_BY_UID_KEY_HEADER'], str(self.uid)])

        pipe = r.pipeline()
        pipe.sadd(es.config['BUCKET_SET'], self.name)
        pipe.sadd(key_path_by_set, self.name)
        pipe.hmset(key_path, self.__dict__)
        pipe.execute()

    def create(self):
        from organize import Directory
        ret = dict()
        ret['state'] = ji.Common.exchange_state(20000)

        if self.exist_by_name():
            ret['state'] = ji.Common.exchange_state(40901)
            ret['state']['sub']['zh-cn'] = ''.join([ret['state']['sub']['zh-cn'], ': bucket: ', self.name])
            raise ji.PreviewingError(json.dumps(ret))

        self.join_at = ji.Common.tus()
        self.save()

        # 初始化bucket的根目录
        directory = Directory()
        directory.create(root=True, bucket=self.name)

    def get_by_name(self):
        ret = dict()
        ret['state'] = ji.Common.exchange_state(20000)

        if not self.exist_by_name():
            ret['state'] = ji.Common.exchange_state(40401)
            ret['state']['sub']['zh-cn'] = ''.join([ret['state']['sub']['zh-cn'], ': bucket: ', self.name])
            raise ji.PreviewingError(json.dumps(ret))

        key_path = ''.join([es.config['BUCKET_KEY_HEADER'], self.name])
        j_data = r.hgetall(key_path)
        for k, v in j_data.items():
            if hasattr(self, k):
                if k in ['uid', 'join_at', 'policy']:
                    setattr(self, k, int(v))
                else:
                    setattr(self, k, v)

    def get_by_uid(self):
        self.previewing_by_tenantry()
        key_path_by_set = ''.join([es.config['BUCKET_SET_BY_UID_KEY_HEADER'], str(self.uid)])

        ret = dict()
        ret['state'] = ji.Common.exchange_state(20000)
        ret['list'] = list()

        for bucket_name in r.smembers(key_path_by_set):
            bucket = Bucket(name=bucket_name)
            bucket.get_by_name()
            ret['list'].append(bucket.__dict__)

        return ret

    def get(self):
        self.get_by_name()

    def update(self, **kwargs):
        self.previewing_by_tenantry()

        for k, v in kwargs.items():
            if k in ['uid', 'name', 'join_at']:
                continue

            if hasattr(self, k):
                setattr(self, k, v)

        self.save()

    def delete(self):
        ret = dict()
        ret['state'] = ji.Common.exchange_state(20000)

        # 根目录不为空不允许删除bucket
        from organize import Directory
        directory = Directory()
        # 每个bucket的根index node号为1
        directory.index_node = 1
        directory.bucket = self.name
        directory.get()
        # 若为空，根目录中仅存根自身的index node号
        if directory.count > 0:
            ret['state'] = ji.Common.exchange_state(41251)
            ret['state']['sub']['zh-cn'] = ''.join([ret['state']['sub']['zh-cn'], ': bucket: ', self.name])
            raise ji.PreviewingError(json.dumps(ret))

        self.previewing()

        if not self.exist_by_name():
            ret['state'] = ji.Common.exchange_state(40401)
            ret['state']['sub']['zh-cn'] = ''.join([ret['state']['sub']['zh-cn'], ': bucket: ', self.name])
            raise ji.PreviewingError(json.dumps(ret))

        key_path = ''.join([es.config['BUCKET_KEY_HEADER'], self.name])
        key_path_by_set = ''.join([es.config['BUCKET_SET_BY_UID_KEY_HEADER'], str(self.uid)])

        directory.delete()

        pipe = r.pipeline()
        pipe.srem(es.config['BUCKET_SET'], self.name)
        pipe.srem(key_path_by_set, self.name)
        pipe.delete(key_path)
        pipe.execute()

    @staticmethod
    def get_all():
        ret = dict()
        ret['state'] = ji.Common.exchange_state(20000)
        ret['list'] = list()

        for bucket_name in r.smembers(es.config['BUCKET_SET']):
            bucket = Bucket(name=bucket_name)
            bucket.get_by_name()
            ret['list'].append(bucket.__dict__)

        return ret
