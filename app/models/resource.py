#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'James Iter'
__date__ = '15/5/31'
__contact__ = 'james.iter.cn@gmail.com'
__copyright__ = '(c) 2015 by James Iter.'

import magic
import exifread
from subprocess import Popen, PIPE
import xmltodict
import json
import re
import base64

from app.initialize import *
from interface import CURD
from rules import Rules

class ResourceBasic(CURD):

    def __init__(self, **kwargs):
        super(ResourceBasic, self).__init__()
        self.object_id = kwargs.get('object_id', '')                 # 对象存储中的对象id(即FastDFS返回的资源id)
        # self.slot = 0        通过方法算得
        self.name = kwargs.get('name', '')
        self.resource_id = kwargs.get('resource_id', '')               # 通过某种算法得出的，在系统中标识的资源id，这里直接取资源的sha1值
        # self.sha1 = ''
        self.upload_date = kwargs.get('upload_date', 0)
        self.mime = kwargs.get('mime', '')
        self.size = kwargs.get('size', 0)
        self.visible = kwargs.get('visible', True)
        self.get_count = kwargs.get('get_count', 0)
        self.uploader_ip = kwargs.get('uploader_ip', '')            # 首个上传者的ip

    def previewing(self):
        args_rules = [
            Rules.OBJECT_ID.value,
            Rules.RESOURCE_NAME.value,
            Rules.RESOURCE_ID.value,
            eval(Rules.UPLOAD_DATE.value),
            Rules.MIME.value,
            Rules.SIZE.value,
            Rules.VISIBLE.value,
            Rules.GET_COUNT.value,
            Rules.UPLOADER_IP.value
        ]

        ji.Check.previewing(args_rules, self.__dict__)

    def save(self):
        self.previewing()

        key_path = ''.join([es.config['RESOURCE_KEY_HEADER'], self.resource_id])

        pipe = r.pipeline()
        pipe.hmset(key_path, self.__dict__)
        pipe.execute()

    def exist(self):
        args_rules = [
            Rules.RESOURCE_ID.value
        ]
        ji.Check.previewing(args_rules, self.__dict__)

        key_path = ''.join([es.config['RESOURCE_KEY_HEADER'], self.resource_id])
        return r.exists(key_path)

    def create(self, **kwargs):

        for k, v in kwargs.items():
            if k in self.__dict__:
                setattr(self, k, v)

        ret = dict()
        ret['state'] = ji.Common.exchange_state(20000)

        if self.exist():
            ret['state'] = ji.Common.exchange_state(40901)
            ret['state']['sub']['zh-cn'] = ''.join([ret['state']['sub']['zh-cn'], ': resource_id: ', self.resource_id])
            raise ji.PreviewingError(json.dumps(ret))

        self.upload_date = ji.Common.tus()
        self.save()

    def get(self):
        ret = dict()
        ret['state'] = ji.Common.exchange_state(20000)

        if not self.exist():
            ret['state'] = ji.Common.exchange_state(40401)
            ret['state']['sub']['zh-cn'] = ''.join([ret['state']['sub']['zh-cn'], ': resource_id: ', self.resource_id])
            raise ji.PreviewingError(json.dumps(ret))

        key_path = ''.join([es.config['RESOURCE_KEY_HEADER'], self.resource_id])
        j_data = r.hgetall(key_path)
        for k, v in j_data.items():
            if hasattr(self, k) or '@' == k[0]:
                # k第一个字符为@的，其v为指定规格资源id
                if k in ['upload_date', 'size', 'get_count']:
                    setattr(self, k, int(v))
                if k in ['visible']:
                    setattr(self, k, bool(v))
                else:
                    setattr(self, k, v)

    def update(self):
        self.save()

    def delete(self):
        ret = dict()
        ret['state'] = ji.Common.exchange_state(20000)

        if not self.exist():
            ret['state'] = ji.Common.exchange_state(40401)
            ret['state']['sub']['zh-cn'] = ''.join([ret['state']['sub']['zh-cn'], ': resource_id: ', self.resource_id])
            raise ji.PreviewingError(json.dumps(ret))

        key_path = ''.join([es.config['RESOURCE_KEY_HEADER'], self.resource_id])

        pipe = r.pipeline()
        pipe.delete(key_path)
        pipe.execute()

    @staticmethod
    def spec_parse(spec=None):
        args_rules = [
            Rules.SPEC.value
        ]

        ji.Check.previewing(args_rules, locals())

        spec = spec.lower()
        sub_spec_s = spec.strip('@').split('@')

        def get_sub_spec_structure(sub_spec):
            """ 对子规格进行参数有效性过滤、分解，最终返回结构化的规格描述
            :param sub_spec: 原始子规格描述字符串
            :return: _k(识别的子规格类型), _v(对应子规格的结构化描述)
            Note: 暂不支持effects、watermark
            """

            kv_pattern_by_basic = '^(\d+)([whqr])$'
            kv_pattern_by_effects = '^(gb)~(\d+x\d+)$|^(cs)~(cineonlog|cmyk|gray|hsl|hwb|ohta|rgb|rec601luma|rec709luma|' \
                                    'rec601ycbcr|rec709ycbcr|transparent|xyz|ycbcr|yiq|ypbpr|yuv)$'
            kv_pattern_by_watermark = '^(text)~(.*)$|' \
                                      '^(font)~(.*)$|' \
                                      '^(font-color)~(red|blue|green|black|white|gray|#[0-9a-f]{6})$|' \
                                      '^(stroke-color)~(red|blue|green|black|white|gray|#[0-9a-f]{6})$|' \
                                      '^(stroke-width)~(\d+(\.\d+)?)$|' \
                                      '^(expand-per)~(\d+)$|' \
                                      '^(gravity)~(nw|n|ne|w|c|e|sw|s|se)$|' \
                                      '^(dissolve)~(\d+)$|' \
                                      '^(image-path)~(.*)$|' \
                                      '^(resize-per)~(\d+)$'

            _k = None
            _v = {}
            sub_spec_detail_s = sub_spec.split('_')

            if re.match(kv_pattern_by_basic, sub_spec_detail_s[0]) is not None:
                _k = 'basic'
                for _item in sub_spec_detail_s:
                    _m = re.match(kv_pattern_by_basic, _item)
                    if _m is not None:
                        _v[_m.groups()[1]] = _m.groups()[0]

            elif 'effects' == sub_spec_detail_s[0]:
                _k = sub_spec_detail_s[0]
                for _item in sub_spec_detail_s:
                    if re.match(kv_pattern_by_effects, _item) is not None:
                        _item_k, _item_v = _item.split('~')
                        _v[_item_k] = _item_v

            elif 'watermark' == sub_spec_detail_s[0]:
                _k = sub_spec_detail_s[0]
                for _item in sub_spec_detail_s:
                    if re.match(kv_pattern_by_watermark, _item) is not None:
                        _item_k, _item_v = _item.split('~')
                        _v[_item_k] = _item_v

                """ 文字水印字符数合理范围，超出范围则作废 """
                if 'text' in _v and 1 > _v['text'].__len__() > 20:
                    return _k, ''

                """ 文字描边粗细值合理范围，超出范围则忽略该项 """
                if 'stroke-width' in _v and 0.1 > _v['stroke-width'] > 10:
                    del _v['stroke-width']

                """ 文字贴图缩放比例的合理范围，超出范围则忽略该项 """
                if 'expand-per' in _v and 1 > _v['expand-per'] > 500:
                    del _v['expand-per']

                """ 贴图不透明度，超出范围则忽略该项 """
                if 'dissolve' in _v and 1 > _v['dissolve'] > 99:
                    del _v['dissolve']

                if 'resize-per' in _v and 1 > _v['resize-per'] > 1000:
                    del _v['resize-per']

                """ text, image-path 相斥，text优先级更高 """
                if 'image-path' in _v:
                    if 'text' in _v:
                        del _v['image-path']
                    else:
                        # 判断图片资源是否存在于当前存储中
                        from app.models import File, Utils
                        try:
                            bucket, path, _spec = Utils.separate_bucket_path_spec(
                                resource_path=base64.b32decode(_v['image-path'].upper()))
                            _file = File()
                            _file.bucket = bucket
                            _file.get_by_path(path=path)
                        except Exception, e:
                            logger.warning(e.message)
                            logger.warning(''.join(['image-path: ', _v['image-path']]))
                            return _k, {}

                        for key in ['font', 'font-color', 'stroke-color', 'stroke-width', 'expand-per']:
                            if key in _v:
                                del _v[key]

            else:
                return _k, {}

            return _k, _v

        _sub_spec_s = {}
        for _sub_spec in sub_spec_s:
            k, v = get_sub_spec_structure(_sub_spec)

            if k is not None:
                _sub_spec_s[k] = v

        return _sub_spec_s

    @staticmethod
    def spec_format(spec):
        """ 对原始规格描述进行有效性过滤、排序，返回指定格式化后的规格描述字符串
        :param spec: 原始规格描述字符串 ('100w_120h_20r_x60_fw123')
        :return: 格式化后的规格描述字符串 ('120h_20r_100w')
        """
        spec_formatted = []
        # 返回结构化的规格描述
        spec_structure = ResourceBasic.spec_parse(spec)

        for item in ['basic', 'effects', 'watermark']:
            if item not in spec_structure:
                continue

            sub_spec_formatted = []
            if item == 'basic':
                spec_basic = spec_structure[item]
                keys = spec_basic.keys()
                # 排序的目的在于，不必被以后新增的参数打乱规格描述的组合规则
                keys.sort()
                for key in keys:
                    sub_spec_formatted.append(''.join([spec_basic[key], key]))

            elif item == 'effects':
                spec_effects = spec_structure[item]
                keys = spec_effects.keys()
                keys.sort()

                sub_spec_formatted.append(item)
                for key in keys:
                    sub_spec_formatted.append(''.join([key, '~', spec_effects[key]]))

            elif item == 'watermark':
                spec_watermark = spec_structure[item]
                keys = spec_watermark.keys()
                keys.sort()

                sub_spec_formatted.append(item)
                for key in keys:
                    sub_spec_formatted.append(''.join([key, '~', spec_watermark[key]]))

            else:
                logger.warning(''.join(['未支持的规格类型: ', str(item)]))

            if sub_spec_formatted.__len__() > 1:
                sub_spec_formatted = '_'.join(sub_spec_formatted)
            elif sub_spec_formatted.__len__() == 1:
                sub_spec_formatted = sub_spec_formatted[0]
            else:
                sub_spec_formatted = ''

            spec_formatted.append(sub_spec_formatted)

        if spec_formatted.__len__() > 1:
            spec_formatted = '@'.join(spec_formatted)

        elif spec_formatted.__len__() == 1:
            spec_formatted = spec_formatted[0]

        else:
            spec_formatted = None

        # 默认返回值为None
        if spec_formatted is not None:
            spec_formatted = ''.join(['@', spec_formatted])
            return spec_formatted.encode('utf8')


class Image(ResourceBasic):

    def __init__(self, **kwargs):
        super(Image, self).__init__(**kwargs)
        self.width = 0
        self.height = 0
        self.longitude = ''
        self.latitude = ''

    def create(self, **kwargs):
        super(Image, self).create(**kwargs)


class Video(ResourceBasic):

    def __init__(self, **kwargs):
        super(Video, self).__init__(**kwargs)
        self.width = 0
        self.height = 0
        self.longitude = ''
        self.latitude = ''
        self.duration = 0

    def create(self, **kwargs):
        super(Video, self).create(**kwargs)


class ResourceChild(CURD):

    def __init__(self, **kwargs):
        super(ResourceChild, self).__init__()
        self.object_id = kwargs.get('object_id', '')
        self.resource_id = kwargs.get('resource_id', '')
        self.upload_date = kwargs.get('upload_date', 0)
        self.size = kwargs.get('size', 0)
        self.ancestor_resource_id = kwargs.get('ancestor_resource_id', '')

    def previewing(self):
        args_rules = [
            Rules.OBJECT_ID.value,
            Rules.RESOURCE_ID.value,
            eval(Rules.UPLOAD_DATE.value),
            Rules.SIZE.value
        ]

        ji.Check.previewing(args_rules, self.__dict__)

    def exist(self):
        args_rules = [
            Rules.RESOURCE_ID.value
        ]
        ji.Check.previewing(args_rules, self.__dict__)

        key_path = ''.join([es.config['RESOURCE_KEY_HEADER'], self.resource_id])
        return r.exists(key_path)

    def save(self):
        self.previewing()

        key_path = ''.join([es.config['RESOURCE_KEY_HEADER'], self.resource_id])

        pipe = r.pipeline()
        pipe.hmset(key_path, self.__dict__)
        pipe.execute()

    def create(self, **kwargs):

        for k, v in kwargs.items():
            if k in self.__dict__:
                setattr(self, k, v)

        ret = dict()
        if self.exist():
            ret['state'] = ji.Common.exchange_state(40901)
            ret['state']['sub']['zh-cn'] = ''.join([ret['state']['sub']['zh-cn'], ': resource_id: ', self.resource_id])
            raise ji.PreviewingError(json.dumps(ret))

        self.upload_date = ji.Common.tus()
        self.save()

    def get(self):
        if not self.exist():
            ret = dict()
            ret['state'] = ji.Common.exchange_state(40401)
            ret['state']['sub']['zh-cn'] = ''.join([ret['state']['sub']['zh-cn'], ': resource_id: ', self.resource_id])
            raise ji.PreviewingError(json.dumps(ret))

        key_path = ''.join([es.config['RESOURCE_KEY_HEADER'], self.resource_id])
        j_data = r.hgetall(key_path)
        for k, v in j_data.items():
            if hasattr(self, k):
                if k in ['upload_date', 'size']:
                    setattr(self, k, int(v))
                else:
                    setattr(self, k, v)

    def update(self):
        self.save()

    def delete(self):
        if not self.exist():
            ret = dict()
            ret['state'] = ji.Common.exchange_state(40401)
            ret['state']['sub']['zh-cn'] = ''.join([ret['state']['sub']['zh-cn'], ': resource_id: ', self.resource_id])
            raise ji.PreviewingError(json.dumps(ret))

        key_path = ''.join([es.config['RESOURCE_KEY_HEADER'], self.resource_id])

        pipe = r.pipeline()
        pipe.delete(key_path)
        pipe.execute()


class MediaInfo(object):

    def __init__(self, medium_path=''):
        self.medium_path = medium_path
        self.size = 0
        self.width = 0
        self.height = 0
        self.duration = 0
        self.longitude = None
        self.latitude = None
        self.mime = ''
        self.exif = dict()
        self.mediainfo = dict()

        args_rules = [
            Rules.MEDIUM_PATH.value
        ]

        ji.Check.previewing(args_rules, self.__dict__)

        if not os.path.isfile(self.medium_path):
            ret = dict()
            ret['state'] = ji.Common.exchange_state(40401)
            ret['state']['zh-cn'] = ''.join([ret['state']['zh-cn'], ': ', self.medium_path])
            raise ji.PreviewingError(json.dumps(ret))

    def get_size(self):
        self.size = os.path.getsize(self.medium_path)

    def get_mime(self):
        self.mime = magic.from_file(self.medium_path, mime=True)

    def get_exif(self):
        with open(self.medium_path, 'rb') as f:
            self.exif = exifread.process_file(f)

    def get_media_info(self):
        command = ''.join(["mediainfo -f --Output=XML ", self.medium_path])
        p = Popen(command, stdout=PIPE, stderr=PIPE, env=os.environ, shell=True)
        p.wait()
        mediainfo_out = ''.join(p.stdout.readlines())
        # TODO: 应当对其做异常捕获，并记录机制
        j_mediainfo_out = xmltodict.parse(mediainfo_out)
        for item in j_mediainfo_out['Mediainfo']['File']['track']:
            if item['@type'] == 'General':
                self.mediainfo['General'] = item
            elif item['@type'] == 'Video':
                self.mediainfo['Video'] = item
            elif item['@type'] == 'Audio':
                self.mediainfo['Audio'] = item

    def get_basic_info(self):
        self.get_size()
        self.get_mime()
        self.get_exif()

        if self.mime in es.config['ALLOWED_IMAGE_MIME']:
            self.width = int(self.exif['EXIF ExifImageWidth'].printable)
            self.height = int(self.exif['EXIF ExifImageLength'].printable)
            self.handle_gps_fields()

        elif self.mime in es.config['ALLOWED_VIDEO_MIME']:
            self.get_media_info()
            self.width = int(self.mediainfo['Video']['Width'][0])
            self.height = int(self.mediainfo['Video']['Height'][0])
            self.duration = int(self.mediainfo['Video']['Duration'][0])

    @staticmethod
    def exchange_gps2degree(dms_gps=None, direction=None):
        args_rules = [
            (list, 'longitude'),
            (list, 'latitude')
        ]
        ji.Check.previewing(args_rules, dms_gps)

        args_rules = [
            (basestring, 'longitude', ['E', 'e', 'W', 'w']),
            (basestring, 'latitude', ['N', 'n', 'S', 's'])
        ]
        ji.Check.previewing(args_rules, direction)

        ret = dict()
        ret['state'] = ji.Common.exchange_state(41203)
        for key in ['longitude', 'latitude']:
            for i, item in enumerate(dms_gps[key]):
                if i == 0 and key == 'longitude':
                    if -180 > item or item > 180:
                        raise ji.PreviewingError(json.dumps(ret))
                else:
                    if -60 > item or item > 60:
                        raise ji.PreviewingError(json.dumps(ret))

        longitude = dms_gps['longitude'][0]
        longitude += (dms_gps['longitude'][1] * 60 + dms_gps['longitude'][2]) / 3600.0

        latitude = dms_gps['latitude'][0]
        latitude += (dms_gps['latitude'][1] * 60 + dms_gps['latitude'][2]) / 3600.0

        if 'w' == direction['longitude'].lower():
            longitude = 0 - longitude

        if 's' == direction['latitude'].lower():
            latitude = 0 - latitude

        return {'longitude': longitude, 'latitude': latitude}

    def handle_gps_fields(self):
        args_rules = [
            (object, 'GPS GPSLongitude'),
            (object, 'GPS GPSLatitude'),
            (object, 'GPS GPSLongitudeRef'),
            (object, 'GPS GPSLatitudeRef')
        ]
        ji.Check.previewing(args_rules, self.exif)

        dms_gps = {'longitude': [], 'latitude': []}
        dms_gps['longitude'].append(
            self.exif['GPS GPSLongitude'].values[0].num / float(self.exif['GPS GPSLongitude'].values[0].den))
        dms_gps['longitude'].append(
            self.exif['GPS GPSLongitude'].values[1].num / float(self.exif['GPS GPSLongitude'].values[1].den))
        dms_gps['longitude'].append(
            self.exif['GPS GPSLongitude'].values[2].num / float(self.exif['GPS GPSLongitude'].values[2].den))

        dms_gps['latitude'].append(
            self.exif['GPS GPSLatitude'].values[0].num / float(self.exif['GPS GPSLatitude'].values[0].den))
        dms_gps['latitude'].append(
            self.exif['GPS GPSLatitude'].values[1].num / float(self.exif['GPS GPSLatitude'].values[1].den))
        dms_gps['latitude'].append(
            self.exif['GPS GPSLatitude'].values[2].num / float(self.exif['GPS GPSLatitude'].values[2].den))

        direction = {
            'longitude': self.exif['GPS GPSLongitudeRef'].values, 'latitude': self.exif['GPS GPSLatitudeRef'].values}
        gps_degree = self.exchange_gps2degree(dms_gps, direction)

        self.longitude = gps_degree['longitude']
        self.latitude = gps_degree['latitude']

