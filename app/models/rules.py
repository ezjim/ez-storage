#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'James Iter'
__date__ = '15/8/22'
__contact__ = 'james.iter.cn@gmail.com'
__copyright__ = '(c) 2015 by James Iter.'


from app.initialize import *
import states
from enum import Enum
import copy


class Rules(Enum):
    UID = ((int, long), 'uid', (es.config['UID_MIN'], es.config['UID_MAX']))
    # LOGIN_NAME = (basestring, 'login_name', (es.config['LOGIN_NAME_LENGTH_MIN'], es.config['LOGIN_NAME_LENGTH_MAX']))
    LOGIN_NAME = (''.join(['regex:', es.config['EMAIL_PATTERN']]), 'login_name')
    PASSWORD = (basestring, 'password', (es.config['PASSWORD_MIN'], es.config['PASSWORD_MAX']))
    PASSWORD_HASH = (basestring, 'password')
    JOIN_AT = "(int, 'join_at', (ji.Common.tus() - 2 * 1000000, ji.Common.tus() + es.config['SESSION_TTL'] * 1000000))"
    LAST_UPDATE_AT = "(int, 'last_update_at', (0, ji.Common.tus() + es.config['SESSION_TTL'] * 1000000))"
    EMAIL = (''.join(['regex:', es.config['EMAIL_PATTERN']]), 'email')

    GID = ((int, long), 'gid', (es.config['GID_MIN'], es.config['GID_MAX']))
    ROLE_GROUP_NAME = (basestring, 'name', (es.config['NORMAL_STRING_FIELD_MIN'], es.config['NORMAL_STRING_FIELD_MAX']))
    ROLE_GROUP_MEMBERS = (list, 'members')
    ROLE_GROUP_PRIVILEGES = (list, 'privileges')

    PRIVILEGE = (basestring, 'privilege', (es.config['NORMAL_STRING_FIELD_MIN'], es.config['NORMAL_STRING_FIELD_MAX']))
    PRIVILEGE_LABEL = (basestring, 'label', (es.config['NORMAL_STRING_FIELD_MIN'],
                                             es.config['NORMAL_STRING_FIELD_MAX']))

    ORGANIZATION_NAME = (basestring, 'organization_name', (es.config['NORMAL_STRING_FIELD_MIN'],
                                                           es.config['NORMAL_STRING_FIELD_MAX']))

    # TODO: bucket 名称只允许ASCII码，不支持特殊字符
    # BUCKET_NAME = (basestring, 'name', (es.config['BUCKET_LENGTH_MIN'], es.config['BUCKET_LENGTH_MAX']))
    BUCKET_NAME = (''.join(['regex:', es.config['BUCKET_NAME_PATTERN']]), 'name')
    BUCKET_POLICY = (int, 'policy', states.BucketPolicy.__members__.values())
    ACCESS_KEY_ID = (basestring, 'id', (es.config['ACCESS_KEY_ID_LENGTH'], es.config['ACCESS_KEY_ID_LENGTH']))
    ACCESS_KEY_SECRET = (basestring, 'secret', (es.config['ACCESS_KEY_SECRET_LENGTH'],
                                                es.config['ACCESS_KEY_SECRET_LENGTH']))

    INDEX_NODE = ((int, long), 'index_node', (es.config['LONG_MIN'], es.config['LONG_MAX']))
    PARENT_INDEX_NODE = ((int, long), 'parent_index_node', (es.config['LONG_MIN'], es.config['LONG_MAX']))
    PATH = (''.join(['regex:', es.config['PATH']]), 'path')
    FILE_TYPE = (int, 'file_type', states.FileType.__members__.values())
    FILE_NAME = (basestring, 'name', (es.config['FILE_NAME_LENGTH_MIN'], es.config['FILE_NAME_LENGTH_MAX']))
    FILE_NAME_BY_ROOT = (basestring, 'name', ['/'])
    DIRECTORY_CHILD = (list, 'child')
    REAL_FILE_KEY = (basestring, 'real_file_key', (len(es.config['RESOURCE_KEY_HEADER']) + 40,
                                                   len(es.config['RESOURCE_KEY_HEADER']) + 40))
    GET_COUNT = ((int, long), 'get_count', (es.config['LONG_MIN'], es.config['LONG_MAX']))

    OBJECT_ID = (basestring, 'object_id')
    RESOURCE_NAME = (basestring, 'name')
    RESOURCE_ID = (basestring, 'resource_id')
    UPLOAD_DATE = "(int, 'upload_date', (0, ji.Common.tus() + es.config['SESSION_TTL'] * 1000000))"
    MIME = (basestring, 'mime')
    SIZE = ((int, long), 'size')
    VISIBLE = (bool, 'visible')
    UPLOADER_IP = (basestring, 'uploader_ip')

    MEDIUM_PATH = list(copy.copy(PATH))
    MEDIUM_PATH[1] = 'medium_path'
    MEDIUM_PATH = tuple(MEDIUM_PATH)

    PAYLOAD = (basestring, 'payload')
    X_ACCESS_KEY_ID = list(copy.copy(ACCESS_KEY_ID))
    X_ACCESS_KEY_ID[1] = 'x-access-key-id'
    X_ACCESS_KEY_ID = tuple(X_ACCESS_KEY_ID)
    X_BUCKET_NAME = list(copy.copy(BUCKET_NAME))
    X_BUCKET_NAME[1] = 'x-bucket-name'
    X_BUCKET_NAME = tuple(X_BUCKET_NAME)

    X_SHA1 = (basestring, 'x-sha1')

    RESOURCE_PATH = (''.join(['regex:', es.config['RESOURCE_PATH_PATTERN']]), 'resource_path')
    SPEC = (''.join(['regex:', es.config['SPEC']]), 'spec')
    ANCESTOR_RESOURCE_ID = list(copy.copy(RESOURCE_ID))
    ANCESTOR_RESOURCE_ID[1] = 'ancestor_resource_id'
    ANCESTOR_RESOURCE_ID = tuple(ANCESTOR_RESOURCE_ID)
