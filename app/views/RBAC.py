#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'James Iter'
__date__ = '15/8/29'
__contact__ = 'james.iter.cn@gmail.com'
__copyright__ = '(c) 2015 by James Iter.'

import json

from flask import Blueprint, request, g
from app.initialize import *
from app.models import RBAC, Utils


blueprint = Blueprint(
    'rbac',
    __name__,
    url_prefix='/rbac'
)

@es.before_request
@Utils.dumps2response
def r_before_request():
    try:
        g.access_control = RBAC.AccessControl()
        g.access_control.endpoint = request.endpoint

        if not g.access_control.is_not_need_to_auth() and request.method != 'OPTIONS':
            # 没有token或无效的token，都将抛出41208异常，告诉客户端重新登陆获取有效token
            g.access_control.token = request.cookies.get('token', '')
            g.access_control.verify_token()

            # token校验通过，AccessControl.account 将初始化，所以可以直接进行权限校验
            # 校验失败将抛出40301异常，告诉客户端未授权
            g.access_control.permission_check()
    except ji.JITError, e:
        return json.loads(e.message)

@es.after_request
@Utils.dumps2response
def r_after_request(response):
    try:
        # TODO: 由配置文件来设定
        # 由于浏览器同源策略，凡是发送请求url的协议、域名、端口三者之间任意一与当前页面地址不同即为跨域。
        response.headers['Access-Control-Allow-Origin'] = 'http://ezs-shell.ez-iso.com'
        response.headers['Access-Control-Allow-Credentials'] = 'true'
        response.headers['Access-Control-Allow-Methods'] = 'HEAD, GET, POST, DELETE, OPTIONS, PATCH, PUT'
        response.headers['Access-Control-Allow-Headers'] = 'X-Request-With, Content-Type'
        response.headers['Access-Control-Expose-Headers'] = 'Set-Cookie'
        return response
    except ji.JITError, e:
        return json.loads(e.message)

@Utils.dumps2response
def r_login():
    try:
        return g.access_control.login(**request.json)
    except ji.JITError, e:
        return json.loads(e.message)

@Utils.dumps2response
def r_logout():
    try:
        return g.access_control.logout()
    except ji.JITError, e:
        return json.loads(e.message)

@Utils.dumps2response
def r_privilege_get_all():
    return RBAC.Privilege.get_all()

@Utils.dumps2response
def r_privilege_create():
    privilege = RBAC.Privilege()
    try:
        privilege.create(**request.json)
    except ji.PreviewingError, e:
        return json.loads(e.message)

@Utils.dumps2response
def r_privilege_delete():
    privilege = RBAC.Privilege()
    privilege.privilege = request.json.get('privilege')     # 取不到值得None，这样下面get()时会抛参数类型错误
    try:
        privilege.get()
        privilege.delete()
    except ji.PreviewingError, e:
        return json.loads(e.message)

@Utils.dumps2response
def r_role_group_create():
    role_group = RBAC.RoleGroup()
    try:
        role_group.create(**request.json)
    except ji.PreviewingError, e:
        return json.loads(e.message)

@Utils.dumps2response
def r_role_group_get_all():
    return RBAC.RoleGroup.get_all()

@Utils.dumps2response
def r_role_group_update():
    role_group = RBAC.RoleGroup(**request.json)
    try:
        role_group.update()
    except ji.PreviewingError, e:
        return json.loads(e.message)

@Utils.dumps2response
def r_role_group_delete():
    role_group = RBAC.RoleGroup()
    role_group.gid = request.json.get('gid')    # 取不到值得None，这样下面get()时会抛参数类型错误
    try:
        role_group.get()
        role_group.delete()
    except ji.PreviewingError, e:
        return json.loads(e.message)
