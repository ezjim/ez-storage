#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'James Iter'
__date__ = '15/9/5'
__contact__ = 'james.iter.cn@gmail.com'
__copyright__ = '(c) 2015 by James Iter.'

import json

from flask import Blueprint, request, g
from app.initialize import *
from app.models import AccessKey
from app.models import Utils


blueprint = Blueprint(
    'access_key',
    __name__,
    url_prefix='/access_key'
)

@Utils.dumps2response
def r_access_key_create():
    access_key = AccessKey()
    access_key.uid = g.access_control.account.uid
    try:
        access_key.create()
    except ji.PreviewingError, e:
        return json.loads(e.message)

@Utils.dumps2response
def r_access_key_get():
    access_key = AccessKey(**request.json)
    try:
        access_key.get()
    except ji.PreviewingError, e:
        return json.loads(e.message)

    ret = dict()
    ret['state'] = ji.Common.exchange_state(20000)
    ret['obj'] = access_key.__dict__
    return ret

@Utils.dumps2response
def r_access_key_get_by_uid():
    access_key = AccessKey()
    access_key.uid = g.access_control.account.uid
    try:
        return access_key.get_by_uid()
    except ji.PreviewingError, e:
        return json.loads(e.message)

@Utils.dumps2response
def r_access_key_update():
    access_key = AccessKey(**request.json)
    access_key.uid = g.access_control.account.uid
    try:
        access_key.update(**request.json)
    except ji.PreviewingError, e:
        return json.loads(e.message)

@Utils.dumps2response
def r_access_key_delete():
    access_key = AccessKey(**request.json)
    access_key.uid = g.access_control.account.uid
    try:
        access_key.get()
        access_key.delete()
    except ji.PreviewingError, e:
        return json.loads(e.message)

@Utils.dumps2response
def r_access_key_get_all():
    """
    方便管理员获取系统中的access_key信息列表
    :return:
    """
    return AccessKey.get_all()
