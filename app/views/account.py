#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'James Iter'
__date__ = '15/8/19'
__contact__ = 'james.iter.cn@gmail.com'
__copyright__ = '(c) 2015 by James Iter.'

import json

from flask import Blueprint, request, g
from app.initialize import *
from app.models import Account, Utils


blueprint = Blueprint(
    'account',
    __name__,
    url_prefix='/account'
)


@Utils.dumps2response
def r_account_register():
    account = Account()
    try:
        account.create(**request.json)
    except ji.PreviewingError, e:
        return json.loads(e.message)

@Utils.dumps2response
def r_account_get():
    """
    获取登陆者自己的账户信息
    :return:
    """
    ret = dict()
    ret['state'] = ji.Common.exchange_state(20000)
    ret['obj'] = g.access_control.account.__dict__
    del ret['obj']['password']
    return ret

@Utils.dumps2response
def r_account_get_all():
    """
    方便管理员获取系统中的用户信息列表
    :return:
    """
    return Account.get_all()
