#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'James Iter'
__date__ = '15/9/9'
__contact__ = 'james.iter.cn@gmail.com'
__copyright__ = '(c) 2015 by James Iter.'

import json

from flask import Blueprint, request, g
from app.initialize import *
from app.models import Utils, IndexNode, Directory, File, Bucket, Rules
from app.models import states


blueprint = Blueprint(
    'organize',
    __name__,
    url_prefix='/organize'
)


def distinguish_organize_object():
    index_node = IndexNode(**request.json)
    index_node.get()

    if index_node.file_type == states.FileType.directory.value:
        organize = Directory(**request.json)

    elif index_node.file_type == states.FileType.file.value:
        organize = File(**request.json)

    else:
        ret = dict()
        ret['state'] = ji.Common.exchange_state(41203)
        raise ji.PreviewingError(json.dumps(ret))

    return organize

def distinguish_organize_object_by_path(bucket=None, path=None):
    index_node = IndexNode()
    index_node.bucket = bucket
    index_node.get_by_path(path=path)

    if index_node.file_type == states.FileType.directory.value:
        organize = Directory()

    elif index_node.file_type == states.FileType.file.value:
        organize = File()

    else:
        ret = dict()
        ret['state'] = ji.Common.exchange_state(41203)
        raise ji.PreviewingError(json.dumps(ret))

    organize.bucket = bucket
    organize.get_by_path(path=path)
    return organize

@Utils.dumps2response
def r_directory_create():
    directory = Directory()
    try:
        directory.create(**request.json)
    except ji.PreviewingError, e:
        return json.loads(e.message)

@Utils.dumps2response
def r_file_create():
    _file = File()
    try:
        _file.create(**request.json)
    except ji.PreviewingError, e:
        return json.loads(e.message)

@Utils.dumps2response
def r_organize_get():
    try:
        organize = distinguish_organize_object()
        organize.get()
    except ji.PreviewingError, e:
        return json.loads(e.message)

    ret = dict()
    ret['state'] = ji.Common.exchange_state(20000)
    ret['obj'] = organize.__dict__
    return ret

@Utils.dumps2response
def r_organize_get_by_name():
    try:
        organize = distinguish_organize_object()
        organize.get_by_name()
    except ji.PreviewingError, e:
        return json.loads(e.message)

    ret = dict()
    ret['state'] = ji.Common.exchange_state(20000)
    ret['obj'] = organize.__dict__
    return ret

@Utils.dumps2response
def r_organize_get_by_path(resource_path=None):
    try:
        bucket, path, spec = Utils.separate_bucket_path_spec(resource_path=resource_path)
        organize = distinguish_organize_object_by_path(bucket=bucket, path=path)
    except ji.PreviewingError, e:
        return json.loads(e.message)

    ret = dict()
    ret['state'] = ji.Common.exchange_state(20000)
    ret['obj'] = organize.__dict__
    return ret

@Utils.dumps2response
def r_organize_delete():
    try:
        organize = distinguish_organize_object()
        organize.get()
        organize.delete()
    except ji.PreviewingError, e:
        return json.loads(e.message)

