#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'James Iter'
__date__ = '15/8/22'
__contact__ = 'james.iter.cn@gmail.com'
__copyright__ = '(c) 2015 by James Iter.'


from flask import Blueprint
from app.initialize import *
from app.models import Utils


blueprint = Blueprint(
    'db',
    __name__,
    url_prefix='/db'
)

@Utils.dumps2response
def r_clear_db():
    if es.config['DEBUG']:
        r.flushdb()
    else:
        ret = dict()
        ret['state'] = ji.Common.exchange_state(40300)
        return ret

