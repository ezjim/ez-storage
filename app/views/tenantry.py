#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'James Iter'
__date__ = '15/8/18'
__contact__ = 'james.iter.cn@gmail.com'
__copyright__ = '(c) 2015 by James Iter.'

import json

from flask import Blueprint, request, g
from app.initialize import *
from app.models import Tenantry
from app.models import Utils


blueprint = Blueprint(
    'tenantry',
    __name__,
    url_prefix='/tenantry'
)

@Utils.dumps2response
def r_tenantry_create():
    tenantry = Tenantry()
    tenantry.uid = g.access_control.account.uid
    try:
        tenantry.create(**request.json)
    except ji.PreviewingError, e:
        return json.loads(e.message)

@Utils.dumps2response
def r_tenantry_update():
    tenantry = Tenantry()
    tenantry.uid = g.access_control.account.uid
    try:
        tenantry.update(**request.json)
    except ji.PreviewingError, e:
        return json.loads(e.message)

@Utils.dumps2response
def r_tenantry_get():
    tenantry = Tenantry()
    tenantry.uid = g.access_control.account.uid
    try:
        tenantry.get()
    except ji.PreviewingError, e:
        return json.loads(e.message)

    ret = dict()
    ret['state'] = ji.Common.exchange_state(20000)
    ret['obj'] = tenantry.__dict__
    return ret

@Utils.dumps2response
def r_tenantry_delete():
    tenantry = Tenantry()
    tenantry.uid = g.access_control.account.uid
    try:
        tenantry.get()
        tenantry.delete()
    except ji.PreviewingError, e:
        return json.loads(e.message)
