#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'James Iter'
__date__ = '15/8/18'
__contact__ = 'james.iter.cn@gmail.com'
__copyright__ = '(c) 2015 by James Iter.'

import json
from time import sleep

from flask import Blueprint, request, make_response
from app.initialize import *
from app.models import File, ResourceBasic, Image, Video, ResourceChild, MediaInfo, Bucket
from app.models import Tenantry, Utils, Rules
from app.models import states


blueprint = Blueprint(
    'resource',
    __name__,
    url_prefix='/resource'
)


@Utils.dumps2response
def r_resource_create():
    # 接收参数的两种渠道: http请求头、http查询参数，http请求头更为优先
    if 'payload' in request.headers:
        args = request.headers
    else:
        args = request.args

    try:
        args_rules = [
            Rules.X_ACCESS_KEY_ID.value,
            Rules.PAYLOAD.value
        ]

        ji.Check.previewing(args_rules, args)

        """ 对递交内容的有效性、合法性进行校验、解析 """
        payload = Tenantry.decode_payload(access_key_id=args['x-access-key-id'], payload=args['payload'])

        args_rules = [
            Rules.X_BUCKET_NAME.value,
            Rules.PARENT_INDEX_NODE.value,
            Rules.X_SHA1.value
        ]

        ret = ji.Check.previewing(args_rules, payload)

        """ 如果已经存在，返回已存在异常 """
        resource_obj = ResourceBasic()
        file_obj = File()

        resource_obj.resource_id = payload['x-sha1']
        resource_obj.uploader_ip = request.environ.get('HTTP_X_REAL_IP', '')

        if resource_obj.exist():
            resource_obj.get()

        else:
            """ 上传真实的资源 """
            _file = request.files['file']

            if not hasattr(_file, 'stream'):
                ret['state'] = ji.Common.exchange_state(41201)
                ret['state']['sub']['zh-cn'] = ''.join([ret['state']['sub']['zh-cn'],
                                                        ': stream in request.files["file"]'])
                raise ji.PreviewingError(json.dumps(ret))

            """ 检测扩展名是否在白名单中 """
            file_obj.name = _file.filename
            if not file_obj.allowed_file():
                ret['state'] = ji.Common.exchange_state(40300)
                raise ji.PreviewingError(json.dumps(ret))

            resource_obj.name = _file.filename

            sha1 = ji.Common.calc_sha1_by_fd(_file.stream)

            if resource_obj.resource_id != sha1:
                ret['state'] = ji.Common.exchange_state(41250)
                raise ji.PreviewingError(json.dumps(ret))

            _file.stream.seek(0, 0)
            tmp_save_path = os.path.join(es.config['TMP_STORAGE_PATH'], sha1[-1], sha1)
            _file.save(tmp_save_path)

            media_info = MediaInfo(medium_path=tmp_save_path)
            media_info.get_basic_info()

            def fill_attribute(obj_major, obj_minor):
                for k, v in obj_minor.__dict__.items():
                    if hasattr(obj_major, k):
                        setattr(obj_major, k, v)

            if media_info.mime in es.config['ALLOWED_IMAGE_MIME']:
                resource_obj = Image(**resource_obj.__dict__)
                fill_attribute(resource_obj, media_info)

            elif media_info.mime in es.config['ALLOWED_VIDEO_MIME']:
                resource_obj = Video()
                fill_attribute(resource_obj, media_info)

            fs_ret = fs_client.upload_by_filename(tmp_save_path)
            resource_obj.object_id = fs_ret['Remote file_id']
            os.remove(tmp_save_path)

            resource_obj.create()

        """ 将真实资源映射到租户的文件系统中 """
        file_obj.create(resource_id=resource_obj.resource_id, uploader_ip=resource_obj.uploader_ip,
                        bucket=payload['x-bucket-name'], parent_index_node=payload['parent_index_node'],
                        name=resource_obj.name)

    except ji.PreviewingError, e:
        return json.loads(e.message)


@Utils.dumps2response
def r_resources_get(resource_path=None):
    try:
        bucket, path, spec = Utils.separate_bucket_path_spec(resource_path=resource_path)
        organize = File()
        organize.bucket = bucket
        organize.get_by_path(path=path)

        key_path = ''.join([es.config['RESOURCE_KEY_HEADER'], organize.resource_id])
        mime = r.hget(key_path, 'mime')

        if mime in es.config['ALLOWED_IMAGE_MIME']:
            resource_obj = Image()
        elif mime in es.config['ALLOWED_VIDEO_MIME']:
            resource_obj = Video()
        else:
            resource_obj = ResourceBasic()

        ret = dict()

        resource_obj.resource_id = organize.resource_id
        resource_obj.get()
        if not resource_obj.visible:
            ret['state'] = ji.Common.exchange_state(40301)
            return ret

        origin_resource_key_path = ''.join([es.config['RESOURCE_KEY_HEADER'], resource_obj.resource_id])

        response = make_response()
        if spec is not None:
            spec_formatted = ResourceBasic.spec_format(spec=spec)

            if spec_formatted is not None:
                # 判断指定规格是否已存在
                if not hasattr(resource_obj, spec_formatted):
                    # 当指定规格不存在时的处理逻辑
                    spec_msg = json.dumps({'resource_id': resource_obj.resource_id, 'spec': spec_formatted})
                    if resource_obj.mime in es.config['ALLOWED_IMAGE_MIME']:
                        queue = es.config['IMAGE_QUEUE_ACCEPT']
                    elif resource_obj.mime in es.config['ALLOWED_VIDEO_MIME']:
                        queue = es.config['VIDEO_QUEUE']
                    else:
                        logger.warning(''.join(['不支持处理的mime类型资源: ',
                                                json.dumps({'resource_id': resource_obj.resource_id,
                                                            'spec': spec_formatted})]))
                        ret['state'] = ji.Common.exchange_state(50150)
                        return ret

                    # 判断是否已经在处理队列当中了，重复请求会产生这种情况
                    if spec_msg not in r.lrange(es.config['IMAGE_QUEUE_ACCEPT'], 0, -1) and \
                            not r.sismember(es.config['IMAGE_PROCESSING_SET'], spec_msg):
                        r.rpush(queue, spec_msg)

                    cycle = 0
                    while True:
                        sleep(0.2)
                        # 直接操作数据库，完全为了性能。这里逻辑清晰，执行次数多，没必须每次循环都去对象流程
                        if r.hexists(origin_resource_key_path, spec_formatted):
                            resource_obj.get()
                            break
                        elif cycle > 25:
                            ret['state'] = ji.Common.exchange_state(20201)
                            return ret

                        cycle += 1

                resource_child = ResourceChild(resource_id=getattr(resource_obj, spec_formatted))
                resource_child.get()
                """ 用子资源的数据元信息，更新原始资源数据元的相应部分 """
                resource_obj.__dict__.update(resource_child.__dict__)

        position = request.headers.get('position', 0)
        size = request.headers.get('size', 0)
        content = fs_client.download_to_buffer(remote_file_id=str(resource_obj.object_id), offset=position,
                                               down_bytes=size)
        response.headers['Content-Type'] = resource_obj.mime
        response.data = content['Content']
        r.hincrby(origin_resource_key_path, 'get_count', 1)
        return response

    except ji.PreviewingError, e:
        return json.loads(e.message)


@Utils.dumps2response
def r_resources_delete():
    pass

