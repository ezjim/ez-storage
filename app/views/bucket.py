#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'James Iter'
__date__ = '15/9/4'
__contact__ = 'james.iter.cn@gmail.com'
__copyright__ = '(c) 2015 by James Iter.'

import json

from flask import Blueprint, request, g
from app.initialize import *
from app.models import Bucket
from app.models import Utils


blueprint = Blueprint(
    'bucket',
    __name__,
    url_prefix='/bucket'
)

@Utils.dumps2response
def r_bucket_create():
    bucket = Bucket(**request.json)
    bucket.uid = g.access_control.account.uid
    try:
        bucket.create()
    except ji.PreviewingError, e:
        return json.loads(e.message)

@Utils.dumps2response
def r_bucket_get():
    bucket = Bucket(**request.json)
    try:
        bucket.get()
    except ji.PreviewingError, e:
        return json.loads(e.message)

    ret = dict()
    ret['state'] = ji.Common.exchange_state(20000)
    ret['obj'] = bucket.__dict__
    return ret

@Utils.dumps2response
def r_bucket_get_by_uid():
    bucket = Bucket()
    bucket.uid = g.access_control.account.uid
    try:
        return bucket.get_by_uid()
    except ji.PreviewingError, e:
        return json.loads(e.message)

@Utils.dumps2response
def r_bucket_update(name=None):
    bucket = Bucket()
    bucket.name = name
    bucket.uid = g.access_control.account.uid
    try:
        bucket.get()
        bucket.update(**request.json)
    except ji.PreviewingError, e:
        return json.loads(e.message)

@Utils.dumps2response
def r_bucket_delete(name=None):
    bucket = Bucket()
    bucket.name = name
    bucket.uid = g.access_control.account.uid
    try:
        bucket.get()
        bucket.delete()
    except ji.PreviewingError, e:
        return json.loads(e.message)

@Utils.dumps2response
def r_bucket_get_all():
    """
    方便管理员获取系统中的bucket信息列表
    :return:
    """
    return Bucket.get_all()
