#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'James Iter'
__date__ = '15/8/19'
__contact__ = 'james.iter.cn@gmail.com'
__copyright__ = '(c) 2015 by James Iter.'

from app.models.utils import add_rule
from app.views import db
from app.views import account
from app.views import tenantry
from app.views import bucket
from app.views import access_key
from app.views import RBAC
from app.views import organize
from app.views import resource

# 数据库相关蓝图
add_rule(db.blueprint, '/clear', view_func='db.r_clear_db', methods=['GET'])

# 账号相关蓝图
add_rule(account.blueprint, '', view_func='account.r_account_register', methods=['POST'])
add_rule(account.blueprint, '', view_func='account.r_account_get', methods=['GET'])
add_rule(account.blueprint, '/_all', view_func='account.r_account_get_all', methods=['GET'])

# 租户相关蓝图
add_rule(tenantry.blueprint, '', view_func='tenantry.r_tenantry_get', methods=['GET'])
add_rule(tenantry.blueprint, '', view_func='tenantry.r_tenantry_create', methods=['POST'])
add_rule(tenantry.blueprint, '', view_func='tenantry.r_tenantry_update', methods=['PATCH'])
add_rule(tenantry.blueprint, '', view_func='tenantry.r_tenantry_delete', methods=['DELETE'])

# bucket相关蓝图
add_rule(bucket.blueprint, '/<path:name>', view_func='bucket.r_bucket_get', methods=['GET'])
add_rule(bucket.blueprint, '/_all', view_func='bucket.r_bucket_get_all', methods=['GET'])
add_rule(bucket.blueprint, '/_by_uid', view_func='bucket.r_bucket_get_by_uid', methods=['GET'])
add_rule(bucket.blueprint, '', view_func='bucket.r_bucket_create', methods=['POST'])
add_rule(bucket.blueprint, '/<path:name>', view_func='bucket.r_bucket_update', methods=['PATCH'])
add_rule(bucket.blueprint, '/<path:name>', view_func='bucket.r_bucket_delete', methods=['DELETE'])

# access_key相关蓝图
add_rule(access_key.blueprint, '', view_func='access_key.r_access_key_get', methods=['GET'])
add_rule(access_key.blueprint, '/_all', view_func='access_key.r_access_key_get_all', methods=['GET'])
add_rule(access_key.blueprint, '/_by_uid', view_func='access_key.r_access_key_get_by_uid', methods=['GET'])
add_rule(access_key.blueprint, '', view_func='access_key.r_access_key_create', methods=['POST'])
add_rule(access_key.blueprint, '', view_func='access_key.r_access_key_update', methods=['PATCH'])
add_rule(access_key.blueprint, '', view_func='access_key.r_access_key_delete', methods=['DELETE'])

# 文件组织相关蓝图
add_rule(organize.blueprint, '', view_func='organize.r_organize_get', methods=['GET'])
add_rule(organize.blueprint, '/_by_name', view_func='organize.r_organize_get_by_name', methods=['GET'])
add_rule(organize.blueprint, '/<path:resource_path>', view_func='organize.r_organize_get_by_path', methods=['GET'])
add_rule(organize.blueprint, '', view_func='organize.r_file_create', methods=['POST'])
add_rule(organize.blueprint, '/_dir', view_func='organize.r_directory_create', methods=['POST'])
add_rule(organize.blueprint, '', view_func='organize.r_organize_delete', methods=['DELETE'])

# 资源相关蓝图
add_rule(resource.blueprint, '', view_func='resource.r_resource_create', methods=['POST'])
add_rule(resource.blueprint, '/<path:resource_path>', view_func='resource.r_resources_get', methods=['GET'])

# 访问控制相关蓝图
add_rule(RBAC.blueprint, '/login', view_func='RBAC.r_login', methods=['POST'])
add_rule(RBAC.blueprint, '/logout', view_func='RBAC.r_logout', methods=['POST'])

add_rule(RBAC.blueprint, '/privilege', view_func='RBAC.r_privilege_get_all', methods=['GET'])
add_rule(RBAC.blueprint, '/privilege', view_func='RBAC.r_privilege_create', methods=['POST'])
add_rule(RBAC.blueprint, '/privilege', view_func='RBAC.r_privilege_delete', methods=['DELETE'])

add_rule(RBAC.blueprint, '/role_group', view_func='RBAC.r_role_group_get_all', methods=['GET'])
add_rule(RBAC.blueprint, '/role_group', view_func='RBAC.r_role_group_create', methods=['POST'])
add_rule(RBAC.blueprint, '/role_group', view_func='RBAC.r_role_group_update', methods=['PATCH'])
add_rule(RBAC.blueprint, '/role_group', view_func='RBAC.r_role_group_delete', methods=['DELETE'])

