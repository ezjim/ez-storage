#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'James Iter'
__date__ = '15/6/17'
__contact__ = 'james.iter.cn@gmail.com'
__copyright__ = '(c) 2015 by James Iter.'

import unittest
from app.models import organize
import jimit as ji


class TestOrganize(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

#     def test_mkdir(self):
#         ret = organize.Organize.mkdir(1L, 'test', ''.join(['/', ji.JITime.today()]))
#         pass

    def test_makedirs(self):
        ret = organize.Organize.makedirs(1L, 'test', ''.join(['/', ji.JITime.today(), '/', ji.JITime.now_time()]))

if __name__ == '__main__':
    unittest.main()
