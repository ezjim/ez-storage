#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'James Iter'
__date__ = '15/5/31'
__contact__ = 'james.iter.cn@gmail.com'
__copyright__ = '(c) 2015 by James Iter.'

import unittest
import requests
import jwt
import jimit as ji
import json
# from app.models import Resource as rs


class TestResource(unittest.TestCase):
    base_url = 'http://ezs.ez-iso.com'
    aud = 'ResourceOperation'
    jwt_algorithm = 'HS512'
    email = 'james.iter.cn@gmail.com'
    password = '000000.com'
    namespace = ''
    uid = ''
    api_key = ''
    bucket = ''
    directory_path = ''

    @classmethod
    def setUpClass(cls):

        url = ''.join([TestResource.base_url, '/db/clear'])
        r = requests.get(url)
        j_r = json.loads(r.text)
        assert '200' == j_r['state']['code']

        dir_s = []
        for i in xrange(3):
            dir_s.append(ji.Common.generate_random_code(length=10))

        cls.directory_path = ''.join(['/', '/'.join(dir_s)])

    @classmethod
    def tearDownClass(cls):
        pass

    @staticmethod
    def test_10_tenantry_post():
        url = ''.join([TestResource.base_url, '/tenantry'])
        r = requests.post(url=url, json={'email': TestResource.email, 'password': TestResource.password})
        j_r = json.loads(r.text)
        assert '200' == j_r['state']['code']
        TestResource.namespace = j_r['namespace']
        TestResource.api_key = j_r['api_key']
        TestResource.uid = j_r['uid']

    @staticmethod
    def test_11_tenantry_make_bucket_post():
        url = ''.join([TestResource.base_url, '/tenantry/bucket'])
        r = requests.post(url=url, json={'uid': TestResource.uid, 'bucket': 'bkt'})
        j_r = json.loads(r.text)
        assert '200' == j_r['state']['code']
        TestResource.bucket = j_r['bucket']

    @staticmethod
    def test_20_post_resource():
        url = 'http://ezs.ez-iso.com/resources'
        # url = 'http://ezs.ez-iso.com/resources'
        # multiple_files = [('resource', ('DSC01723.JPG', open('/Users/James/Documents/2015-02-24/DSC01723.JPG', 'rb'), 'image/jpg'))]
        # files = {'file': ('DSC01723.JPG', open('/Users/James/Documents/2015-02-24/DSC01723.JPG', 'rb'), 'multipart/form-data')}
        files = {'file': ('2015-02-14.jpg', open('/Users/James/Documents/2015-02-14.jpg', 'rb'), 'multipart/form-data')}
        # files = {'file': ('DSC_1583.MOV', open('/Users/James/Documents/DSC_1583.MOV', 'rb'), 'multipart/form-data')}
        # r = requests.post(url, files=multiple_files)
        payload = {
            # 'iat': ji.Common.ts(),                                                  # 创建于
            # 'nbf': ji.Common.ts(),                                                  # 在此之前不可用
            # 'exp': ji.Common.ts() + es.config['INVITATION_TENANTRY_TOKEN_TTL'],     # 过期时间
            'aud': TestResource.aud,                   # 受众
            'bucket': TestResource.bucket,
            'directory_path': TestResource.directory_path
        }
        payload = jwt.encode(payload=payload, key=TestResource.api_key, algorithm=TestResource.jwt_algorithm)
        r = requests.post(url, files=files,
                          headers={'namespace': TestResource.namespace, 'payload': payload})
        j_r = json.loads(r.text)
        print r.text
        assert '200' == j_r['state']['code']

"""
    @staticmethod
    def test_30_spec_format():
        f_str = rs.spec_format('100w_120h_20r_x60_fw123')
        print f_str
        f_str = rs.spec_format('@100W_120h_20r_x60_fw123@effEcts_fds~32x2_cs~gray_gb~5x5')
        print f_str
"""

if __name__ == '__main__':
    unittest.main()
