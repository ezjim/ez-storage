#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'James Iter'
__date__ = '15/8/22'
__contact__ = 'james.iter.cn@gmail.com'
__copyright__ = '(c) 2015 by James Iter.'

import requests


def clear_db():
    url = 'http://ezs.ez-iso.com/db/clear'
    r = requests.get(url)
    print r.text.encode('utf-8')
