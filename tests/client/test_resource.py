#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'James Iter'
__date__ = '15/9/18'
__contact__ = 'james.iter.cn@gmail.com'
__copyright__ = '(c) 2015 by James Iter.'

import unittest
import requests
import json
import jwt
import jimit as ji

from tests.client import clear_db


class TestResource(unittest.TestCase):

    cookies_by_user01_admin = None
    bucket_name = 'es-debug'
    access_key = None
    jwt_algorithm = 'HS512'

    def setUp(self):
        pass

    def tearDown(self):
        pass

    # ------------------------------------------------初始化---------------------------------------------------------
    # 用户、租户、bucket初始化
    def test_10_init(self):
        url = 'http://ezs.ez-iso.com/account'
        r = requests.post(url=url, json={'login_name': 'james.iter.cn@gmail.com', 'password': '000000.com'})
        print r.text.encode('utf-8')
        self.assertEqual('200', json.loads(r.text)['state']['code'])

        # 登陆获取在cookie中的token
        url = 'http://ezs.ez-iso.com/rbac/login'
        r = requests.post(url=url, json={'login_name': 'james.iter.cn@gmail.com', 'password': '000000.com'})
        print r.text.encode('utf-8')
        TestResource.cookies_by_user01_admin = r.cookies
        self.assertEqual('200', json.loads(r.text)['state']['code'])

        # 创建租户
        url = 'http://ezs.ez-iso.com/tenantry'
        r = requests.post(url=url, cookies=TestResource.cookies_by_user01_admin, json={'organization_name': '浩海霸储'})
        print r.text.encode('utf-8')
        self.assertEqual('200', json.loads(r.text)['state']['code'])

        # 创建Bucket
        url = 'http://ezs.ez-iso.com/bucket'
        r = requests.post(url=url, cookies=TestResource.cookies_by_user01_admin, json={'name': TestResource.bucket_name})
        print r.text.encode('utf-8')

        # 创建access_key对
        url = 'http://ezs.ez-iso.com/access_key'
        r = requests.post(url=url, cookies=TestResource.cookies_by_user01_admin)
        print r.text.encode('utf-8')
        self.assertEqual('200', json.loads(r.text)['state']['code'])

        # 获取用户的access_key
        url = 'http://ezs.ez-iso.com/access_key/_by_uid'
        r = requests.get(url=url, cookies=TestResource.cookies_by_user01_admin)
        print r.text.encode('utf-8')
        TestResource.access_key = json.loads(r.text)['list'][0]
        self.assertEqual('200', json.loads(r.text)['state']['code'])

    # ------------------------------------------------上传---------------------------------------------------------
    # 上传图片
    def test_15_upload_image(self):
        url = 'http://ezs.ez-iso.com/resource'
        file_path = '/Users/James/Documents/2015-02-14.jpg'
        files = {'file': ('2015-02-14.jpg', open(file_path, 'rb'), 'multipart/form-data')}
        payload = {
            'parent_index_node': 1,
            'x-bucket-name': TestResource.bucket_name,
            'x-sha1': ji.Common.calc_sha1_by_file(file_path)
        }
        payload = jwt.encode(payload=payload, key=TestResource.access_key['secret'],
                             algorithm=TestResource.jwt_algorithm)
        r = requests.post(url=url, files=files, cookies=TestResource.cookies_by_user01_admin,
                          headers={'x-access-key-id': TestResource.access_key['id'], 'payload': payload})
        print r.text.encode('utf-8')
        self.assertEqual('200', json.loads(r.text)['state']['code'])

clear_db()

if __name__ == '__main__':
    unittest.main()
