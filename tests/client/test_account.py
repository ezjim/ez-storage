#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'James Iter'
__date__ = '15/8/22'
__contact__ = 'james.iter.cn@gmail.com'
__copyright__ = '(c) 2015 by James Iter.'

import unittest
import requests
import json

def clear_db():
    url = 'http://ezs.ez-iso.com/db/clear'
    r = requests.get(url)
    print r.text.encode('utf-8')

class TestAccount(unittest.TestCase):

    cookies_by_user01_admin = None
    cookies_by_user02 = None
    cookies_by_user03 = None

    def setUp(self):
        pass

    def tearDown(self):
        pass

    # ------------------------------------------------注册、登陆---------------------------------------------------------
    # 用户注册
    def test_10_register(self):
        url = 'http://ezs.ez-iso.com/account'
        r = requests.post(url=url, json={'login_name': 'james.iter.cn@gmail.com', 'password': '000000.com'})
        print r.text.encode('utf-8')
        self.assertEqual('200', json.loads(r.text)['state']['code'])

        # 新用户注册
        url = 'http://ezs.ez-iso.com/account'
        r = requests.post(url=url, json={'login_name': 'jamesiter@163.com', 'password': '000000.com'})
        print r.text.encode('utf-8')
        self.assertEqual('200', json.loads(r.text)['state']['code'])

        # 新用户注册
        url = 'http://ezs.ez-iso.com/account'
        r = requests.post(url=url, json={'login_name': 'jamesiter@126.com', 'password': '000000.com'})
        print r.text.encode('utf-8')
        self.assertEqual('200', json.loads(r.text)['state']['code'])

    # 用户重复注册
    def test_11_register(self):
        url = 'http://ezs.ez-iso.com/account'
        r = requests.post(url=url, json={'login_name': 'james.iter.cn@gmail.com', 'password': '000000.com'})
        print r.text.encode('utf-8')
        self.assertEqual('40901', json.loads(r.text)['state']['sub']['code'])

    # 用户登陆
    def test_15_login(self):
        # 账密不匹配
        url = 'http://ezs.ez-iso.com/rbac/login'
        r = requests.post(url=url, json={'login_name': 'james.iter.cn@gmail.com', 'password': 'what?pswd'})
        print r.text.encode('utf-8')
        TestAccount.cookies_by_user01_admin = r.cookies
        self.assertEqual('401', json.loads(r.text)['state']['code'])

        # 账密匹配
        url = 'http://ezs.ez-iso.com/rbac/login'
        r = requests.post(url=url, json={'login_name': 'james.iter.cn@gmail.com', 'password': '000000.com'})
        print r.text.encode('utf-8')
        TestAccount.cookies_by_user01_admin = r.cookies
        self.assertEqual('200', json.loads(r.text)['state']['code'])

        # uid为2的用户登陆，获取他的cookie
        url = 'http://ezs.ez-iso.com/rbac/login'
        r = requests.post(url=url, json={'login_name': 'jamesiter@163.com', 'password': '000000.com'})
        print r.text.encode('utf-8')
        TestAccount.cookies_by_user02 = r.cookies
        self.assertEqual('200', json.loads(r.text)['state']['code'])

        # uid为3的用户登陆，获取他的cookie
        url = 'http://ezs.ez-iso.com/rbac/login'
        r = requests.post(url=url, json={'login_name': 'jamesiter@126.com', 'password': '000000.com'})
        print r.text.encode('utf-8')
        TestAccount.cookies_by_user03 = r.cookies
        self.assertEqual('200', json.loads(r.text)['state']['code'])

    # 获取用户信息
    def test_16_get(self):
        url = 'http://ezs.ez-iso.com/account'
        r = requests.get(url=url, cookies=TestAccount.cookies_by_user01_admin)
        print r.text.encode('utf-8')
        self.assertEqual('200', json.loads(r.text)['state']['code'])

    # 获取用户信息列表
    def test_16_get_all(self):
        url = 'http://ezs.ez-iso.com/account/_all'
        r = requests.get(url=url, cookies=TestAccount.cookies_by_user01_admin)
        print r.text.encode('utf-8')
        self.assertEqual('200', json.loads(r.text)['state']['code'])

    # TODO: 用户同时多处登陆(待考虑)

    # ------------------------------------------------权限---------------------------------------------------------
    # 获取站点所有权限
    def test_20_privilege_get_all(self):
        url = 'http://ezs.ez-iso.com/rbac/privilege'
        r = requests.get(url=url, cookies=TestAccount.cookies_by_user01_admin)
        print r.text.encode('utf-8')
        self.assertEqual('200', json.loads(r.text)['state']['code'])

    # 标识权限 label
    def test_25_privilege_create(self):
        url = 'http://ezs.ez-iso.com/rbac/privilege'
        r = requests.post(url=url, cookies=TestAccount.cookies_by_user01_admin, json={'privilege': 'db.r_clear_db',
                                                                                      'label': u'清除数据库'})
        print r.text.encode('utf-8')
        self.assertEqual('200', json.loads(r.text)['state']['code'])

        # 获取标识后的站点所有权限
        r = requests.get(url=url, cookies=TestAccount.cookies_by_user01_admin)
        print r.text.encode('utf-8')
        self.assertEqual('200', json.loads(r.text)['state']['code'])
        # 判断目标权限是否标识成功
        hit = False
        for item in json.loads(r.text)['list']:
            if item['privilege'] == 'db.r_clear_db':
                hit = True
                self.assertEqual(u'清除数据库', item['label'])
        # 防止漏过
        self.assertEqual(True, hit)

        # 标识相同 label 的权限
        url = 'http://ezs.ez-iso.com/rbac/privilege'
        r = requests.post(url=url, cookies=TestAccount.cookies_by_user01_admin, json={'privilege': 'db.r_clear_db',
                                                                                      'label': u'清除数据库'})
        print r.text.encode('utf-8')
        self.assertEqual('40901', json.loads(r.text)['state']['sub']['code'])

        # 标识新权限的 label
        url = 'http://ezs.ez-iso.com/rbac/privilege'
        r = requests.post(url=url, cookies=TestAccount.cookies_by_user01_admin, json={
            'privilege': 'rbac.r_privilege_get_all', 'label': u'获取站点所有权限列表'})
        print r.text.encode('utf-8')
        self.assertEqual('200', json.loads(r.text)['state']['code'])

    # 删除权限 label
    def test_26_privilege_delete(self):
        url = 'http://ezs.ez-iso.com/rbac/privilege'
        r = requests.delete(url=url, cookies=TestAccount.cookies_by_user01_admin, json={'privilege': 'db.r_clear_db'})
        print r.text.encode('utf-8')
        self.assertEqual('200', json.loads(r.text)['state']['code'])

        # 获取删除指定标识后的站点所有权限
        r = requests.get(url=url, cookies=TestAccount.cookies_by_user01_admin)
        print r.text.encode('utf-8')
        self.assertEqual('200', json.loads(r.text)['state']['code'])
        # 判断目标权限是否标识成功
        hit = False
        for item in json.loads(r.text)['list']:
            if item['privilege'] == 'db.r_clear_db':
                hit = True
                self.assertEqual(None, item['label'])
        # 防止漏过
        self.assertEqual(True, hit)

    # ------------------------------------------------角色组---------------------------------------------------------
    # 创建角色组
    def test_30_role_group_create(self):
        url = 'http://ezs.ez-iso.com/rbac/role_group'
        r = requests.post(url=url, cookies=TestAccount.cookies_by_user01_admin,
                          json={'name': '超级管理员组', 'members': [2], 'privileges': ['rbac.r_privilege_get_all']})
        print r.text.encode('utf-8')
        self.assertEqual('200', json.loads(r.text)['state']['code'])

    # 获取所有角色组
    def test_31_role_group_get_all(self):
        url = 'http://ezs.ez-iso.com/rbac/role_group'
        r = requests.get(url=url, cookies=TestAccount.cookies_by_user01_admin)
        print r.text.encode('utf-8')
        self.assertEqual('200', json.loads(r.text)['state']['code'])

    # 测试用户权限
    def test_32_role_group_no_privilege_user(self):
        # 有权操作
        url = 'http://ezs.ez-iso.com/rbac/privilege'
        r = requests.get(url=url, cookies=TestAccount.cookies_by_user02)
        print r.text.encode('utf-8')
        self.assertEqual('200', json.loads(r.text)['state']['code'])

        # 无权操作
        url = 'http://ezs.ez-iso.com/rbac/privilege'
        r = requests.get(url=url, cookies=TestAccount.cookies_by_user03)
        print r.text.encode('utf-8')
        self.assertEqual('403', json.loads(r.text)['state']['code'])

    # 更新角色组
    def test_35_role_group_update(self):
        # 有权操作
        url = 'http://ezs.ez-iso.com/rbac/role_group'
        r = requests.patch(url=url, cookies=TestAccount.cookies_by_user01_admin,
                           json={"privileges": ["rbac.r_privilege_get_all"], "gid": 1, "name": "管理员组",
                                 "members": [3]})
        print r.text.encode('utf-8')
        self.assertEqual('200', json.loads(r.text)['state']['code'])

        # 有权操作
        url = 'http://ezs.ez-iso.com/rbac/privilege'
        r = requests.get(url=url, cookies=TestAccount.cookies_by_user03)
        print r.text.encode('utf-8')
        self.assertEqual('200', json.loads(r.text)['state']['code'])

    # 删除角色组
    def test_36_role_group_delete(self):
        url = 'http://ezs.ez-iso.com/rbac/role_group'
        r = requests.delete(url=url, cookies=TestAccount.cookies_by_user01_admin, json={'gid': 1})
        print r.text.encode('utf-8')
        self.assertEqual('200', json.loads(r.text)['state']['code'])

        # 获取删除指定role_group后的站点所有角色组
        url = 'http://ezs.ez-iso.com/rbac/role_group'
        r = requests.get(url=url, cookies=TestAccount.cookies_by_user01_admin)
        print r.text.encode('utf-8')
        self.assertEqual('200', json.loads(r.text)['state']['code'])

        # 判断目标角色组是否删除成功
        hit = False
        for item in json.loads(r.text)['list']:
            if 'gid' in item and item['gid'] == 1:
                hit = True

        # 击中即删除失败
        self.assertEqual(False, hit)

        # 无权操作
        url = 'http://ezs.ez-iso.com/rbac/privilege'
        r = requests.get(url=url, cookies=TestAccount.cookies_by_user03)
        print r.text.encode('utf-8')
        self.assertEqual('403', json.loads(r.text)['state']['code'])

    # ------------------------------------------------租户---------------------------------------------------------
    # 创建租户
    def test_40_tenantry_create(self):
        url = 'http://ezs.ez-iso.com/tenantry'
        r = requests.post(url=url, cookies=TestAccount.cookies_by_user01_admin, json={'organization_name': '浩海霸储'})
        print r.text.encode('utf-8')
        self.assertEqual('200', json.loads(r.text)['state']['code'])

    # 获取租户
    def test_45_tenantry_get(self):
        url = 'http://ezs.ez-iso.com/tenantry'
        r = requests.get(url=url, cookies=TestAccount.cookies_by_user01_admin)
        print r.text.encode('utf-8')
        self.assertEqual('200', json.loads(r.text)['state']['code'])

    # 获取更新
    def test_50_tenantry_update(self):
        url = 'http://ezs.ez-iso.com/tenantry'
        r = requests.patch(url=url, cookies=TestAccount.cookies_by_user01_admin, json={'organization_name': '宙贮'})
        print r.text.encode('utf-8')
        self.assertEqual('200', json.loads(r.text)['state']['code'])

        url = 'http://ezs.ez-iso.com/tenantry'
        r = requests.get(url=url, cookies=TestAccount.cookies_by_user01_admin)
        print r.text.encode('utf-8')
        self.assertEqual('200', json.loads(r.text)['state']['code'])

    # 删除租户
    def test_55_tenantry_get(self):
        url = 'http://ezs.ez-iso.com/tenantry'
        r = requests.delete(url=url, cookies=TestAccount.cookies_by_user01_admin)
        print r.text.encode('utf-8')
        self.assertEqual('200', json.loads(r.text)['state']['code'])

        url = 'http://ezs.ez-iso.com/tenantry'
        r = requests.get(url=url, cookies=TestAccount.cookies_by_user01_admin)
        print r.text.encode('utf-8')
        self.assertEqual('404', json.loads(r.text)['state']['code'])

    # ------------------------------------------------Bucket---------------------------------------------------------
    # 创建Bucket
    def test_60_bucket_create(self):
        # 创建租户
        url = 'http://ezs.ez-iso.com/tenantry'
        r = requests.post(url=url, cookies=TestAccount.cookies_by_user01_admin, json={'organization_name': '浩海霸储'})
        print r.text.encode('utf-8')
        self.assertEqual('200', json.loads(r.text)['state']['code'])

        url = 'http://ezs.ez-iso.com/bucket'
        r = requests.post(url=url, cookies=TestAccount.cookies_by_user01_admin, json={'name': 'es-debug'})
        print r.text.encode('utf-8')
        self.assertEqual('200', json.loads(r.text)['state']['code'])

    # 获取Bucket
    def test_62_bucket_get(self):
        url = 'http://ezs.ez-iso.com/bucket'
        r = requests.get(url=url, cookies=TestAccount.cookies_by_user01_admin, json={'name': 'es-debug'})
        print r.text.encode('utf-8')
        self.assertEqual('200', json.loads(r.text)['state']['code'])

    # 更新Bucket
    def test_65_bucket_update(self):
        url = 'http://ezs.ez-iso.com/bucket'
        r = requests.patch(url=url, cookies=TestAccount.cookies_by_user01_admin, json={'name': 'es-debug', 'policy': 2})
        print r.text.encode('utf-8')
        self.assertEqual('200', json.loads(r.text)['state']['code'])

        url = 'http://ezs.ez-iso.com/bucket'
        r = requests.get(url=url, cookies=TestAccount.cookies_by_user01_admin, json={'name': 'es-debug'})
        print r.text.encode('utf-8')
        self.assertEqual('200', json.loads(r.text)['state']['code'])

    # 获取全部Bucket
    def test_67_bucket_get_all(self):
        url = 'http://ezs.ez-iso.com/bucket/_all'
        r = requests.get(url=url, cookies=TestAccount.cookies_by_user01_admin)
        print r.text.encode('utf-8')
        self.assertEqual('200', json.loads(r.text)['state']['code'])

    # 删除Bucket
    def test_70_bucket_delete(self):
        url = 'http://ezs.ez-iso.com/bucket'
        r = requests.delete(url=url, cookies=TestAccount.cookies_by_user01_admin, json={'name': 'es-debug'})
        print r.text.encode('utf-8')
        self.assertEqual('200', json.loads(r.text)['state']['code'])

        url = 'http://ezs.ez-iso.com/bucket'
        r = requests.get(url=url, cookies=TestAccount.cookies_by_user01_admin, json={'name': 'es-debug'})
        print r.text.encode('utf-8')
        self.assertEqual('404', json.loads(r.text)['state']['code'])

    # ------------------------------------------------AccessKey---------------------------------------------------------
    # 创建AccessKey
    def test_75_access_key_create(self):
        url = 'http://ezs.ez-iso.com/access_key'
        r = requests.post(url=url, cookies=TestAccount.cookies_by_user01_admin)
        print r.text.encode('utf-8')
        self.assertEqual('200', json.loads(r.text)['state']['code'])

        # 创建第二个AccessKey对象
        url = 'http://ezs.ez-iso.com/access_key'
        r = requests.post(url=url, cookies=TestAccount.cookies_by_user01_admin)
        print r.text.encode('utf-8')
        self.assertEqual('200', json.loads(r.text)['state']['code'])

    # 获取uid所有的Access_key
    def test_77_access_key_get_by_uid(self):
        url = 'http://ezs.ez-iso.com/access_key/_by_uid'
        r = requests.get(url=url, cookies=TestAccount.cookies_by_user01_admin)
        print r.text.encode('utf-8')
        self.assertEqual('200', json.loads(r.text)['state']['code'])

        # 获取Access_key
        for item in json.loads(r.text)['list']:
            url = 'http://ezs.ez-iso.com/access_key'
            r = requests.get(url=url, cookies=TestAccount.cookies_by_user01_admin, json={'id': item['id']})
            print r.text.encode('utf-8')
            self.assertEqual('200', json.loads(r.text)['state']['code'])

    # 获取系统中所有的Access_key
    def test_80_access_key_get_all(self):
        url = 'http://ezs.ez-iso.com/access_key/_all'
        r = requests.get(url=url, cookies=TestAccount.cookies_by_user01_admin)
        print r.text.encode('utf-8')
        self.assertEqual('200', json.loads(r.text)['state']['code'])

        # 删除Access_key
        for item in json.loads(r.text)['list']:
            url = 'http://ezs.ez-iso.com/access_key'
            r = requests.delete(url=url, cookies=TestAccount.cookies_by_user01_admin, json={'id': item['id']})
            print r.text.encode('utf-8')
            self.assertEqual('200', json.loads(r.text)['state']['code'])

clear_db()

if __name__ == '__main__':
    unittest.main()
