#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'James Iter'
__date__ = '15/9/10'
__contact__ = 'james.iter.cn@gmail.com'
__copyright__ = '(c) 2015 by James Iter.'

import unittest
import requests
import json

from tests.client import clear_db


class TestOrganize(unittest.TestCase):

    cookies_by_user01_admin = None
    bucket_name = 'es-debug'
    resource_id = '4a0a19218e082a343a1b17e5333409af9d98f0f5'

    def setUp(self):
        pass

    def tearDown(self):
        pass

    # ------------------------------------------------初始化---------------------------------------------------------
    # 用户、租户、bucket初始化
    def test_10_init(self):
        url = 'http://ezs.ez-iso.com/account'
        r = requests.post(url=url, json={'login_name': 'james.iter.cn@gmail.com', 'password': '000000.com'})
        print r.text.encode('utf-8')
        self.assertEqual('200', json.loads(r.text)['state']['code'])

        # 登陆获取在cookie中的token
        url = 'http://ezs.ez-iso.com/rbac/login'
        r = requests.post(url=url, json={'login_name': 'james.iter.cn@gmail.com', 'password': '000000.com'})
        print r.text.encode('utf-8')
        TestOrganize.cookies_by_user01_admin = r.cookies
        self.assertEqual('200', json.loads(r.text)['state']['code'])

        # 创建租户
        url = 'http://ezs.ez-iso.com/tenantry'
        r = requests.post(url=url, cookies=TestOrganize.cookies_by_user01_admin, json={'organization_name': '浩海霸储'})
        print r.text.encode('utf-8')
        self.assertEqual('200', json.loads(r.text)['state']['code'])

        # 创建Bucket
        url = 'http://ezs.ez-iso.com/bucket'
        r = requests.post(url=url, cookies=TestOrganize.cookies_by_user01_admin, json={'name': TestOrganize.bucket_name})
        print r.text.encode('utf-8')
        self.assertEqual('200', json.loads(r.text)['state']['code'])

    # 创建目录
    def test_20_directory_create(self):
        url = 'http://ezs.ez-iso.com/organize/_dir'
        r = requests.post(url=url, cookies=TestOrganize.cookies_by_user01_admin,
                          json={'bucket': TestOrganize.bucket_name, 'name': 'first_dir', 'parent_index_node': 1})
        print r.text.encode('utf-8')
        self.assertEqual('200', json.loads(r.text)['state']['code'])

        # 创建同名目录名
        url = 'http://ezs.ez-iso.com/organize/_dir'
        r = requests.post(url=url, cookies=TestOrganize.cookies_by_user01_admin,
                          json={'bucket': TestOrganize.bucket_name, 'name': 'first_dir', 'parent_index_node': 1})
        print r.text.encode('utf-8')
        self.assertEqual('409', json.loads(r.text)['state']['code'])

    def test_25_file_create(self):
        url = 'http://ezs.ez-iso.com/organize'
        r = requests.post(url=url, cookies=TestOrganize.cookies_by_user01_admin,
                          json={'bucket': TestOrganize.bucket_name, 'name': 'first_file.jpg', 'parent_index_node': 1,
                                'resource_id': TestOrganize.resource_id})
        print r.text.encode('utf-8')
        self.assertEqual('200', json.loads(r.text)['state']['code'])

        # 创建同名文件
        url = 'http://ezs.ez-iso.com/organize'
        r = requests.post(url=url, cookies=TestOrganize.cookies_by_user01_admin,
                          json={'bucket': TestOrganize.bucket_name, 'name': 'first_file.jpg', 'parent_index_node': 1,
                                'resource_id': TestOrganize.resource_id})
        print r.text.encode('utf-8')
        self.assertEqual('409', json.loads(r.text)['state']['code'])

        # 创建同名目录
        url = 'http://ezs.ez-iso.com/organize/_dir'
        r = requests.post(url=url, cookies=TestOrganize.cookies_by_user01_admin,
                          json={'bucket': TestOrganize.bucket_name, 'name': 'first_file.jpg', 'parent_index_node': 1,
                                'resource_id': TestOrganize.resource_id})
        print r.text.encode('utf-8')
        self.assertEqual('409', json.loads(r.text)['state']['code'])

        # 在子目录中创建文件
        url = 'http://ezs.ez-iso.com/organize'
        r = requests.post(url=url, cookies=TestOrganize.cookies_by_user01_admin,
                          json={'bucket': TestOrganize.bucket_name, 'name': 'first_file.jpg', 'parent_index_node': 2,
                                'resource_id': TestOrganize.resource_id})
        print r.text.encode('utf-8')
        self.assertEqual('200', json.loads(r.text)['state']['code'])

    def test_30_directory_get(self):
        # 获取根目录
        url = 'http://ezs.ez-iso.com/organize'
        r = requests.get(url=url, cookies=TestOrganize.cookies_by_user01_admin,
                         json={'bucket': TestOrganize.bucket_name, 'index_node': 1})
        print r.text.encode('utf-8')
        self.assertEqual('200', json.loads(r.text)['state']['code'])

        # 遍历获取根目录下的对象
        for item in json.loads(r.text)['obj']['child']:
            url = 'http://ezs.ez-iso.com/organize'
            r = requests.get(url=url, cookies=TestOrganize.cookies_by_user01_admin,
                             json={'bucket': TestOrganize.bucket_name, 'index_node': item['index_node']})
            print r.text.encode('utf-8')
            self.assertEqual('200', json.loads(r.text)['state']['code'])

    def test_31_organize_get_by_path(self):
        # 正常获取
        url = 'http://ezs.ez-iso.com/organize/es-debug/first_dir/first_file.jpg@100x'
        r = requests.get(url=url, cookies=TestOrganize.cookies_by_user01_admin)
        print r.text.encode('utf-8')
        self.assertEqual('200', json.loads(r.text)['state']['code'])

        # 获取不存在的路径
        url = 'http://ezs.ez-iso.com/organize/es-debug/first_dir/not_exist.jpg'
        r = requests.get(url=url, cookies=TestOrganize.cookies_by_user01_admin)
        print r.text.encode('utf-8')
        self.assertEqual('404', json.loads(r.text)['state']['code'])

        # 路径中的目录dir_not_exist目录不存在
        url = 'http://ezs.ez-iso.com/organize/es-debug/first_dir/dir_not_exist/file.jpg'
        r = requests.get(url=url, cookies=TestOrganize.cookies_by_user01_admin)
        print r.text.encode('utf-8')
        self.assertEqual('404', json.loads(r.text)['state']['code'])

        # 获取目录
        url = 'http://ezs.ez-iso.com/organize/es-debug/first_dir'
        r = requests.get(url=url, cookies=TestOrganize.cookies_by_user01_admin)
        print r.text.encode('utf-8')
        self.assertEqual('200', json.loads(r.text)['state']['code'])

        # 路径中的first_file.jpg其实是文件，本应该是目录，会提示其未非目录
        url = 'http://ezs.ez-iso.com/organize/es-debug/first_dir/first_file.jpg/file.jpg'
        r = requests.get(url=url, cookies=TestOrganize.cookies_by_user01_admin)
        print r.text.encode('utf-8')
        self.assertEqual('404', json.loads(r.text)['state']['code'])

    def test_35_organize_delete(self):
        # 在有内容的情况下直接删除目录
        url = 'http://ezs.ez-iso.com/organize'
        r = requests.delete(url=url, cookies=TestOrganize.cookies_by_user01_admin,
                            json={'bucket': TestOrganize.bucket_name, 'index_node': 2})
        print r.text.encode('utf-8')
        self.assertEqual('412', json.loads(r.text)['state']['code'])

        # 删除文件
        url = 'http://ezs.ez-iso.com/organize'
        r = requests.delete(url=url, cookies=TestOrganize.cookies_by_user01_admin,
                            json={'bucket': TestOrganize.bucket_name, 'index_node': 7})
        print r.text.encode('utf-8')
        self.assertEqual('200', json.loads(r.text)['state']['code'])

        # 删除目录
        url = 'http://ezs.ez-iso.com/organize'
        r = requests.delete(url=url, cookies=TestOrganize.cookies_by_user01_admin,
                            json={'bucket': TestOrganize.bucket_name, 'index_node': 2})
        print r.text.encode('utf-8')
        self.assertEqual('200', json.loads(r.text)['state']['code'])

        # 获取根目录
        url = 'http://ezs.ez-iso.com/organize'
        r = requests.get(url=url, cookies=TestOrganize.cookies_by_user01_admin,
                         json={'bucket': TestOrganize.bucket_name, 'index_node': 1})
        print r.text.encode('utf-8')
        self.assertEqual('200', json.loads(r.text)['state']['code'])

clear_db()

if __name__ == '__main__':
    unittest.main()
