#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'James Iter'
__date__ = '15/8/22'
__contact__ = 'james.iter.cn@gmail.com'
__copyright__ = '(c) 2015 by James Iter.'


import json
import unittest

from app.initialize import *
from app.views import account, db


def clear_db():
    ctx = es.test_request_context()
    ctx.push()
    rep = db.r_clear_db()
    ctx.pop()
    assert 200, rep.status_code


class TestAccount(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_01_register_b(self):
        ctx = es.test_request_context(content_type='application/json',
                                      data=json.dumps({'login_name': 'james.iter.cn@gmail.com',
                                                       'password': '000000.com'}))
        ctx.push()
        rep = account.r_register()
        ctx.pop()
        self.assertEqual(200, rep.status_code)

    def test_02_register_again(self):
        ctx = es.test_request_context(content_type='application/json',
                                      data=json.dumps({'login_name': 'james.iter.cn@gmail.com',
                                                       'password': '000000.com'}))
        ctx.push()
        rep = account.r_register()
        ctx.pop()
        self.assertEqual(409, rep.status_code)


clear_db()

if __name__ == '__main__':
    unittest.main()
