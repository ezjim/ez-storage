#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'James Iter'
__date__ = '15/7/2'
__contact__ = 'james.iter.cn@gmail.com'
__copyright__ = '(c) 2015 by James Iter.'


import unittest
from app.models import Email


class TestOrganize(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_email(self):
        email = Email(receivers=['James Iter <james.iter.cn@gmail.com>'], title='Test mail', message='Hello!')
        ret = email.send()
        self.assertEqual('200', ret['state']['code'])

if __name__ == '__main__':
    unittest.main()
